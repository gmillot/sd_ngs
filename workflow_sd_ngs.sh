#!/bin/bash

# Script inspired from Elodie Girard's and Leo Colmet-Daage' scripts. Many thanks to them.
# echo -e "\nJOB COMMAND EXECUTED:\n$0\n" # to get the line that executes the job but does not work (gives /bioinfo/guests/gmillot/Gael_code/workflow_fastq_gael.sh)

set -e # stop the workflow if a command return something different from 0 in $?. Use set -ex to have the blocking line. It is important to know that each instruction executed send a result in $? Either 0 (true) or something else (different from 0)

SCRIPT_USED=$0 # SCRIPT_USED="" or SCRIPT_USED= (it is the same) produce an empty variable. SCRIPT_USED=`echo "$@"` keep only the options, not the .sh before the options ($0).
for i in $@ ; do
	SCRIPT_USED="$SCRIPT_USED $i"
done
SCRIPT_USED=$(echo $SCRIPT_USED | sed 's/ /__/g') # replace space by 2 underscores in SCRIPT_USED



# http://stackoverflow.com/questions/255898/how-to-iterate-over-arguments-in-bash-script

usage () {
cat << EOF

USAGE: `basename $0` -f read1_fastq1[,read1_fastq2...]  -r read2_fastq1[,read2_fastq2...]  -o outdir

Use comma  as a argument separator in options (ie, -f read1_fastq1,read1_fastq2)
Only paired-end fastq files processed, not single-end files
Names of the bam generated will be the character string before the first dot of the fastq file (eg,  G75T01_test.bam for G75T01_test.R1.fastq)

OPTIONS:
	-h	help
	-f	read 1 (forward) of paired-end fastq files(s) or bam file or variant calling format (vcf) file. Files must finish by .fastq (lowercase) or .fastq.gz (lowercase) or .bam or .vcf. Mandatory option
	-r	only for read 2 (reverse) of paired-end fastq files(s). Files must finish by .fastq (lowercase) or .fastq.gz (lowercase). Mandatory option for fastq files
	-s	sample name (s). Must be one name per bam file in -b. Optional argument. If empty, takes the name of the fastq read1 or bam file or vcf file
	-o	output directory. Mandatory option. Either a single path (mandatory for fastq files), or the number of path that correspond to the number of files specified in -f

	-c	--config_file	name and path of the .conf file. Optional only if config_for_NGS_workflow_gael.conf is in /bioinfo/guests/gmillot/Gael_code
	-p	--process_file	name and path of the .sh process file (.sh file that generates raw bam for subsequent analysis). Mandatory

	-L	--l_qsub	qsub -l ressource list option. Use comma without space as separator (default nodes=$NB_NOD_CONF:ppn=$NB_THREADS_CONF,mem=25gb,walltime=2:00:00). BEWARE: modify nodes and ppn in the conf file
	-Q	--q_qsub	qsub -q queue name option (default batch)
	-J	--j_qsub	qsub -j join option (default oe)
	-O	--o_qsub	qsub -o path option (default $OUTDIR/PROCESS_LOG_MULTITASK). Optional but either a single path, or the number of path that correspond to the number of files specified in -o. If nothing specified, use -o and  create $OUTDIR/PROCESS_LOG_MULTITASK
	-M	--m_qsub	qsub -m mail option (default ae)
	-U	--M_qsub	qsub -M user list name option (default \$MAIL)
	-N	--N_qsub	qsub -N job name option for output names. Must be one name per read1 file (in -f). Optional argument. If empty, takes the \${name of the read1 file}_fastq_process
	-T	--t_qsub	qsub -t job array option (1 Mandatory, ie no chromo spliting possible, as in workflow_bam_gael.sh. Otherwise, the job fails)
	-V	--v_qsub	qsub -v local variable option. Declare here supplemental variables that will be used in the fastq_process.sh file (other than defined in -f, -r, -o, -c, -R, and -I)

OPTIONS FOR THE PROCESS FILE:
	-K	--running_operation
			- on a FASTQ file:	'fastqc' - Fastq Quality Control
								'mapping' - using BOWTIE2
			- on a BAM file:	'stats' - mapping and coverage statistics
								'mapqfilter' - removal of bam with low mapq
								'reanalysis' - GATK reanalysis
								'coverage' - GATK coverage
								'calling' - Variant calling
			- on a VCF file:	'annotation' - variant annotation using Annovar
			Optional (default: perform everything). Separate arguments using comma
	-R	--ref_genome			.fa reference genome (path and file) used in BOWTIE2 tools in fastq_process.sh file. Optional (If empty, will use the HG19 indexed genome /data/annotations/Galaxy/Human/hg19/bowtie2/hg19.fa, as defined in the .conf file)
	-A	--ref_genome_name		type of the ref genome specified in -R. Can be "hg19", "hg18", etc... Critical for the annotation operation. Mandatory if -R speficied. If empty, will use "hg19" as defined in the .conf file)
	-B	--bed_file_targeted_ref		.bed file used as targeted reference genome (path and file). Optional. BEWARE: If -R not specified and -B specified, -R will be the hg19 ref genome (-B must come from this genome)
	-m 	--remove_tm_bam_files		logical option: Remove temporary files ? Only 0 or 1 admitted. Default true (0).
	
EX1:  `basename $0`  -f ~/read1_fastq1,~/read1_fastq2  -r ~/read2_fastq1,~/read2_fastq2  -o outdir  -p ~/process.sh -K fastqc,mapping,stats
EX2:  `basename $0`  -f ~/bam1,~/bam2 -o outdir  -p ~/process.sh -K stats,mapqfilter,reanalysis,coverage,calling,annotation
EX3:  `basename $0`  -f ~/vcf1,~/vcf2 -o outdir1,outdir2  -p ~/process.sh -K annotation

EOF
}

function verif {
	if [[ $1 = -* ]]; then
	((OPTIND--))
	fi
}

while getopts “:hf:r:s:o:c:p:L:Q:J:O:M:U:N:T:V:K:R:A:B:m:” OPTION ; do 
	case $OPTION in 
		h)	usage; exit 1 ;; 
		f)	verif $OPTARG ; FASTQ_READ1_INI=$OPTARG ;;
		r)	verif $OPTARG ; FASTQ_READ2_INI=$OPTARG ;;
		s)	verif $OPTARG ; SAMPLE_NAME=$OPTARG ;; # variable used in variantCalling_gael_20160922.sh
		o)	verif $OPTARG ; OUTDIR=$OPTARG  ;;
		c)	verif $OPTARG ; CONFIG_FILE=$OPTARG  ;;
		p)	verif $OPTARG ; FASTQ_PROCESS_FILE=$OPTARG  ;;
		L)	verif $OPTARG ; l_QSUB=$OPTARG  ;;
		Q)	verif $OPTARG ; q_QSUB=$OPTARG  ;;
		J)	verif $OPTARG ; j_QSUB=$OPTARG  ;;
		O)	verif $OPTARG ; o_QSUB=$OPTARG  ;;
		M)	verif $OPTARG ; m_QSUB=$OPTARG  ;;
		U)	verif $OPTARG ; M_QSUB=$OPTARG  ;;
		N)	verif $OPTARG ; N_QSUB=$OPTARG  ;;
		T)	verif $OPTARG ; t_QSUB=$OPTARG  ;;
		V)	verif $OPTARG ; v_QSUB=$OPTARG  ;;
		K)	verif $OPTARG ; RUNNING_OP=$OPTARG  ;;
		R)	verif $OPTARG ; REF_GENOME=$OPTARG  ;;
		A)	verif $OPTARG ; REF_GENOME_TYPE=$OPTARG  ;;
		B)	verif $OPTARG ; BED_FILE=$OPTARG  ;;
		m)	verif $OPTARG ; REMOVE_TMP_BAM=$OPTARG  ;;
		\?)  echo -e "### ERROR ### INVALID OPTION: - $OPTARG\n"  ;  usage; exit 1;;
		:)  echo "### ERROR ### OPTION -$OPTARG REQUIRES AN ARGUMENT\n" >&2 usage; exit 1;;
		esac
done
shift $((OPTIND-1))

################ CHECK


if [[ -z $FASTQ_READ1_INI ]] || [[ -z $OUTDIR ]] || [[ -z $FASTQ_PROCESS_FILE ]]; then
	if [[ -z $FASTQ_READ1_INI ]] ; then
		echo -e "\n### ERROR ###  -f OPTION MUST BE FILLED\n"
	fi
	if  [[ -z $OUTDIR ]]; then
		echo -e "\n### ERROR ###  -o OPTION (output directory) MISSING\n"
	fi
	if  [[ -z $FASTQ_PROCESS_FILE ]]; then
		echo -e "\n### ERROR ###  -p OPTION (.sh process file) MISSING\n"
	fi
	usage
	exit 1
fi


FASTQ_READ1_NAME=( ${FASTQ_READ1_INI//\,/ } ) # replace comma by space and convert into array
read1Num=$(( ${#FASTQ_READ1_NAME[@]} - 1 )) 

if  [[ -z "$SAMPLE_NAME" ]]; then 
	SAMPLE_NAME=() # empty array
	for ((i=0; i<=$read1Num; i++)); do
		if [[ ! ("${FASTQ_READ1_NAME[$i]: -6}" == ".fastq" || "${FASTQ_READ1_NAME[$i]: -9}" == ".fastq.gz" || "${FASTQ_READ1_NAME[$i]: -4}" == ".bam" || "${FASTQ_READ1_NAME[$i]: -4}" == ".vcf") ]] ; then # echo "ok"; fi
			echo -e "\n### ERROR ### NO PROPER .fastq OR .bam OR .vcf FILE EXTENSION: ${FASTQ_READ1_NAME[$i]}\n";
			usage;
			exit 1;
		else
			FASTQ_READ1_NAME_wo_path_tempo=${FASTQ_READ1_NAME[$i]##*/} # idem to =$(basename "${FASTQ_READ1_NAME}"). Remove path. See ${parameter##word} in https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html#Shell-Parameter-Expansion
			IFS='.' read -ra array_tempo2 <<< "$FASTQ_READ1_NAME_wo_path_tempo"
			# system that split a character string into an array according to different delimiters (here dot). Use IFS='_.' for _ and . split, and use \ for special characters, like \\ for \)
			# IFS stands for "internal field separator". See http://unix.stackexchange.com/questions/184863/what-is-the-meaning-of-ifs-n-in-bash-scripting
			unset IFS # set IFS explicitly to this default value. We can also put the literal characters space, tab, newline in single quotes: IFS=$' \t\n'
			SAMPLE_NAME[$i]=${array_tempo2[0]} # take the first part of the name of the fastq which should be the sample name
		fi
	done
else
	SAMPLE_NAME=( ${SAMPLE_NAME//\,/ } )
fi
sampleNum=$(( ${#SAMPLE_NAME[@]} - 1 )) 
if [[ $sampleNum -ne $read1Num ]]; then # -ne means is not equivalent to (see operators of comparison: http://tldp.org/LDP/abs/html/comparison-ops.html ).
	echo -e "\n### ERROR ### NUMBER OF sample names IN -s OPTION ($sampleNum) != NUMBER OF .fastq OR .bam FILES ($read1Num) IN -f OPTION\n"
	usage
	exit 1
fi

if [[ ! -z $FASTQ_READ2_INI ]] ; then
	FASTQ_READ2_NAME=( ${FASTQ_READ2_INI//\,/ } )
	read2Num=$(( ${#FASTQ_READ2_NAME[@]}  - 1 )) 
	for ((i=0; i<=$read2Num; i++)); do
		if [[ ! ("${FASTQ_READ1_NAME[$i]: -6}" == ".fastq" || "${FASTQ_READ1_NAME[$i]: -9}" == ".fastq.gz") ]] ; then # echo "ok"; fi
			echo -e "\n### ERROR ### NO PROPER .fastq FILE EXTENSION in -r OPTION WHEN -f OPTION IS A .fastq FILE: ${FASTQ_READ1_NAME[$i]}\n";
			usage;
			exit 1;
		fi
	done
	if [[ $read1Num -ne $read2Num ]]; then
		echo -e "\n### ERROR ### NUMBER OF read1 FILES IN -f OPTION ($read1Num) != NUMBER OF read2 FILES IN -r OPTION ($read2Num)\n"
		echo -e "  \$FASTQ_READ1_INI:\t$FASTQ_READ1_INI"
		echo -e "  \$FASTQ_READ2_INI:\t$FASTQ_READ2_INI\n"
		usage
		exit 1
	fi
fi

if [[ -z $RUNNING_OP ]]; then 
	RUNNING_OP="fastqc,mapping,stats,mapqfilter,reanalysis,coverage,calling,annotation"
else
	if [[ ! $RUNNING_OP =~ fastqc|mapping|stats|mapqfilter|reanalysis|coverage|calling|annotation ]]; then
		echo -e "\n### ERROR ###  -K OPTION WITH INVALID ARGUMENT. ONLY fastqc,mapping,stats,reanalysis,calling,annotation AUTHORIZED: $RUNNING_OP\n"
		usage
		exit 1
	else
		echo -e "\nFOLLOWING OPERATIONS WILL BE EXECUTED (-K OPTION): $RUNNING_OP\n"
	fi
fi
if [[ $RUNNING_OP =~ fastqc|mapping && ! ( "${FASTQ_READ1_NAME[$1]: -6}" == ".fastq" || "${FASTQ_READ1_NAME[$1]: -9}" == ".fastq.gz" ) ]]; then # Beware: second value of FASTQ_READ1_NAME taken but all them all should be the same
	echo -e "\n### ERROR ### NO fastqc OR mapping RUNNING OPERATION POSSIBLE IN -K OPTION WHEN -f OPTION IS A .bam FILE: $RUNNING_OP\n";
	usage;
	exit 1;
fi
if [[ ( ! $RUNNING_OP =~ mapping ) && ( "${FASTQ_READ1_NAME[$1]: -6}" == ".fastq" || "${FASTQ_READ1_NAME[$1]: -9}" == ".fastq.gz" ) && $RUNNING_OP =~ stats|reanalysis|calling|annotation ]]; then # Beware: second value of FASTQ_READ1_NAME taken but all them all should be the same
	echo -e "\n### ERROR ### NO stats|reanalysis|calling|annotation RUNNING OPERATION POSSIBLE IN -K OPTION WHEN -f OPTION IS A .fastq FILE AND mapping NOT SPECIFIED IN THE -K OPTION: $RUNNING_OP\n";
	usage;
	exit 1;
fi
if [[ $RUNNING_OP == "annotation"  && "${FASTQ_READ1_NAME[$1]: -4}" != ".vcf" ]]; then # Beware: second value of FASTQ_READ1_NAME taken but all them all should be the same
	echo -e "\n### ERROR ### NO annotation RUNNING OPERATION POSSIBLE IN -K OPTION WHEN -f OPTION IS NOT A .vcf FILE\n";
	usage;
	exit 1;
fi

OUTDIR_NAME=( ${OUTDIR//\,/ } )
outdirNum=$(( ${#OUTDIR_NAME[@]} - 1 ))
if [[ ! ($outdirNum == 0 || $outdirNum == $read1Num) ]] ; then # 0 means 1 because first element in array
	echo -e "\n### ERROR ### THE NUMBER OF OUTPUT DIRECTORIES SPECIFIED IN THE -o OPTION MUST BE 1 OR MUST BE THE NUMBER OF FILES SPECIFIED IN THE -f OPTION: $OUTDIR\n"
	usage
	exit 1
elif [[ $outdirNum != 0 && $RUNNING_OP =~ fastqc|mapping ]] ; then # 0 means 1 because first element in array
	echo -e "\n### ERROR ### THE NUMBER OF OUTPUT DIRECTORIES SPECIFIED IN THE -o OPTION MUST BE 1 WHEN THE -K OPTION INCLUDES fastqc OR mapping (FASTQ FILES):\n-o: $OUTDIR\n-K: $RUNNING_OP\n"
	usage
	exit 1
fi
for ((i=0; i<=outdirNum; i++)); do
	if [[ ! -d ${OUTDIR_NAME[$i]} ]]; then
		echo -e "\n### ERROR ### OUTPUT DIRECTORY SPECIFIED IN THE -o OPTION MISSING: ${OUTDIR_NAME[$i]}\n";
		usage;
		exit 1;
	fi
done




if [[ ! -z $CONFIG_FILE ]] ; then
	if [[ ! -f $CONFIG_FILE ]]; then 
		echo -e "\n### ERROR ###  -c OPTION (config file) MISSING AT THE INDICATED PATH\n"
		usage
		exit 1
	fi
else
	if [[ ! -f /bioinfo/guests/gmillot/Gael_code/config_for_NGS_workflow_gael.conf ]]; then
		echo -e "\n### ERROR ###  -c OPTION (config file): config_for_NGS_workflow_gael.conf NOT PRESENT IN /bioinfo/guests/gmillot/Gael_code\n"
		usage
		exit 1
	else
		CONFIG_FILE=/bioinfo/guests/gmillot/Gael_code/config_for_NGS_workflow_gael.conf
	fi
fi
source $CONFIG_FILE

# required conf files in this script
if [[ -z $REF_GENOME_CONF || -z $REF_GENOME_TYPE_CONF || -z $NB_THREADS_CONF || -z $NB_NOD_CONF || -z $MAIL_CONF ]] ; then
	echo -e "\n### ERROR ### AT LEAST ONE OF THE FOLLOWING FILES OR DIRECTORY PATH MISSING OR UNCORRECTLY DEFINED IN config_for_NGS_workflow_gael.conf:"
else
	echo -e "\nFOLLOWING REQUIRED FILES OR DIRECTORIES DETECTED IN THE config_for_NGS_workflow_gael.conf FILE:"
fi
echo -e "  \$REF_GENOME_CONF:\t$REF_GENOME_CONF"
echo -e "  \$REF_GENOME_TYPE_CONF:\t$REF_GENOME_TYPE_CONF"
echo -e "  \$NB_NOD_CONF:\t$NB_NOD_CONF"
echo -e "  \$NB_THREADS_CONF:\t$NB_THREADS_CONF"
echo -e "  \$MAIL_CONF:\t$MAIL_CONF"
if [[ -z $REF_GENOME_CONF || -z $REF_GENOME_TYPE_CONF || -z $NB_THREADS_CONF || -z $NB_NOD_CONF || -z $MAIL_CONF ]] ; then
	exit 1
fi

if [[ ( ! ( $RUNNING_OP =~ fastqc|mapping|reanalysis|annotation ) ) && $NB_THREADS_CONF -ne 1 ]] ; then
	echo -e "\nNO fastqc|mapping|reanalysis|annotation RUNNING OPERATION SPECIFIED BY THE USER. THE NB OF THREADS, DEFINED IN config_for_NGS_workflow_gael.conf (NB_THREADS_CONF=$NB_THREADS_CONF) DOES NOT NEED TO BE MORE THAN 1 AND IS CONVERTED TO 1"
	NB_THREADS_CONF=1
fi
RUNNING_OP=$(echo $RUNNING_OP | sed 's/,/__/g') # replace comma by 2 underscores in RUNNING_OP


if [[ ! -z $BED_FILE ]] && [[ -z $REF_GENOME ]]; then
	echo -e "\nBEWARE: -R OPTION MISSING WHEREAS -B OPTION HAS BEEN SPECIFIED, MEANING THAT THE BED FILE IS DERIVED FROM THE hg19 GENOME\n"
fi

if [[ ! -z $BED_FILE ]] ; then
	TEMPO1=(${BED_FILE//\,/ }) # replace comma by space and convert into array
	if [[ ${#TEMPO1[@]} > 1 ]] ; then
		echo -e "\n### ERROR ### ONLY ONE GENOME AUTHORIZED IN THE -B OPTION: $BED_FILE\n"
		usage
		exit 1
	elif [[ "${BED_FILE: -4}" != ".bed" ]] ; then
		echo -e "\n### ERROR ###  -B OPTION MUST BE A .bed FILE: $BED_FILE\n"
		usage
		exit 1
	fi
fi

if [[ -z $REF_GENOME ]] ; then
	if [[ ! -f $REF_GENOME_CONF ]]; then
		echo -e "\n### ERROR ###  -R DEFAULT OPTION hg19.fa NOT PRESENT IN $REF_GENOME_CONF. CHECK THE config_for_NGS_workflow_gael.conf FILE\n"
		usage
		exit 1
	else
		REF_GENOME=$REF_GENOME_CONF
	fi
fi
TEMPO2=(${REF_GENOME//\,/ }) # replace comma by space and convert into array
if [[ ${#TEMPO2[@]} > 1 ]] ; then
	echo -e "\n### ERROR ###  ONLY ONE GENOME AUTHORIZED IN THE -R OPTION (template reference genome): $REF_GENOME\n"
	usage
	exit 1
elif [[ ! -f $REF_GENOME ]]; then
	echo -e "\n### ERROR ###  -R OPTION SPECIFIED IS NOT PRESENT IN $REF_GENOME\n"
	usage
	exit 1
elif [[ !("${REF_GENOME: -3}" == ".fa" || "${REF_GENOME: -6}" == ".fasta") ]]; then 
	echo -e "\n### ERROR ###  -R OPTION MUST BE A .fa OR .fasta FILE: $REF_GENOME\n"
	usage
	exit 1
fi
REF_GENOME_USED="THE $REF_GENOME FILE WAS USED AS REFERENCE GENOME"
echo -e "\n$REF_GENOME_USED\n"
REF_GENOME_USED=$(echo $REF_GENOME_USED | sed 's/ /__/g') # replace space by 2 underscores in $REF_GENOME_USED


REF_GENOME_PATH=$(dirname "${REF_GENOME}")
REF_GENOME_NAME=${REF_GENOME%.*} # remove extension
REF_GENOME_NAME_WO_PATH=${REF_GENOME_NAME##*/}

#Check for what is it required
if [[ ! -f ${REF_GENOME_NAME}.fa.fai ]]; then
	echo -e "\n### ERROR ### fasta FILE ${REF_GENOME_NAME_WO_PATH}.fa NOT INDEXED: ${REF_GENOME_NAME}.fa.fai FILE DOES NOT EXISTS. USE workflow_genome_processing.sh\n"
	exit 1
fi
if [[ ! -f ${REF_GENOME_NAME}.dict ]] ; then
	echo -e "\n### ERROR ### fasta FILE ${REF_GENOME_NAME_WO_PATH}.fa NOT INDEXED: .dict FILE DOES NOT EXISTS. USE workflow_genome_processing.sh\n"
	exit 1
fi

##### Required for bowtie2 alignment
COUNT_LOOP=0
for i in /$REF_GENOME_PATH/*; do
	if [[ $i =~ .1.bt2|.2.bt2|.3.bt2|.4.bt2|.rev.1.bt2|.rev.2.bt2.|.fa$ ]]; then # .fa$ with $ to specify that this is the end of (keep fa but remove fasta from comparison)
		(( COUNT_LOOP=$COUNT_LOOP + 1 )) # +1 incrementation. See http://askubuntu.com/questions/385528/how-to-increment-a-variable-in-bash
	fi
done
if [[ $COUNT_LOOP == 7 ]]; then
	echo -e "\nTHE REFERENCE GENOME SPECIFIED ${REF_GENOME_NAME_WO_PATH}.fa IS INDEXED (REQUIRED BY bowtie2).\n"
else
	echo -e "\n### ERROR ### THE REFERENCE GENOME SPECIFIED ${REF_GENOME_NAME_WO_PATH}.fa IS NOT INDEXED (REQUIRED BY bowtie2). USE workflow_genome_processing.sh\n"
	usage
	exit 1
fi

if [[ ! -z $REMOVE_TMP_BAM ]]; then 
	if [[ $REMOVE_TMP_BAM -ne 0 ]] && [[ $REMOVE_TMP_BAM -ne 1 ]]; then
		echo -e "\n### ERROR ###  -m LOGICAL OPTION REQUIRES VALUES 0 (true) OR 1 (false) ONLY: $REMOVE_TMP_BAM\n"
		usage
		exit 1
	fi
else
	REMOVE_TMP_BAM=0 #  remove bams
fi


##### Required for annovar
if  [[ -z $REF_GENOME_TYPE ]] ; then
	REF_GENOME_TYPE=$REF_GENOME_TYPE_CONF
fi
IFS='._' read -r -a TEMPO3 <<< "$REF_GENOME_TYPE" # split the character string according to . or _
if [[ ( ${#TEMPO3[@]} > 1 || ${#REF_GENOME_TYPE} > 4 ) && $RUNNING_OP =~ annotation ]]; then
	echo -e "\n### CRITICAL PROBLEM ### REF_GENOME_TYPE SPECIFIED IN -A OPTION OR IN THE .conf FILE DOES NOT SEEM ADAPTED TO ANNOVAR REQUIREMENT: $REF_GENOME_TYPE\n"
	exit 1
fi

################ END CHECK



################ QSUB PARAMETERS



if  [[ -z $N_QSUB ]] ; then
	N_QSUB=("${SAMPLE_NAME[@]}")
else
	n_qsub_Num = $(( ${#N_QSUB[@]} - 1 ))
	if [[ $read1Num -ne $n_qsub_Num ]]; then # -test if the number of values in N_QSUB is identical to the number of bam.
		echo -e "\n### ERROR ### NUMBER OF sample names IN -N OPTION ($read1Num) != NUMBER OF fastq read1 FILES IN -f OPTION ($n_qsub_Num)\n"
		usage
		exit 1
	fi
fi


if  [[ -z $l_QSUB ]] ; then
	l_QSUB="nodes=$NB_NOD_CONF:ppn=$NB_THREADS_CONF,mem=25gb,walltime=2:00:00" # beware: require the source of the conf file for $NB_THREADS_CONF
else
	if [[ $l_QSUB =~ ppn|nodes ]] ; then
		echo -e "\n### ERROR ###  -L OPTION DECLARED WITH nodes OR ppn WHEREAS THIS HAS TO BE SET IN THE config_for_NGS_workflow_gael.conf FILE\n"
		usage
		exit 1
	else
		l_QSUB="nodes=$NB_NOD_CONF:ppn=$NB_THREADS_CONF,$l_QSUB"
	fi
fi

if  [[ -z $q_QSUB ]] ; then
	q_QSUB="batch"
fi

if  [[ -z $j_QSUB ]] ; then
	j_QSUB="oe"
fi

if  [[ ! -z $o_QSUB ]] ; then # -O option specified
	O_QSUB_NAME=( ${o_QSUB//\,/ } ) # array
	oqsubNum=$(( ${#O_QSUB_NAME[@]} - 1 ))
	if [[ ! ($oqsubNum == 0 || $oqsubNum == $read1Num) ]] ; then # 0 means 1 because first element in array
		echo -e "\n### ERROR ### THE NUMBER OF DIRECTORIES SPECIFIED IN THE -O OPTION MUST BE 1 OR MUST BE THE NUMBER OF FILES SPECIFIED IN THE -f OPTION: $o_QSUB\n"
		usage
		exit 1
	elif [[ $oqsubNum != 0 && $RUNNING_OP =~ fastqc|mapping ]] ; then # 0 means 1 because first element in array
		echo -e "\n### ERROR ### THE NUMBER OF OUTPUT DIRECTORIES SPECIFIED IN THE -o OPTION MUST BE 1 WHEN THE -K OPTION INCLUDES fastqc OR mapping (FASTQ FILES):\n-o: $OUTDIR\n-K: $RUNNING_OP\n"
		usage
		exit 1
	fi
	for ((i=0; i<=oqsubNum; i++)); do
		if [[ ! -d ${O_QSUB_NAME[$i]} ]]; then
			echo -e "\n### ERROR ### OUTPUT DIRECTORY SPECIFIED IN THE -O OPTION MISSING: ${O_QSUB_NAME[$i]}\n";
			usage;
			exit 1;
		fi
	done
else # no -O option specified
	O_QSUB_NAME=() # empty array
	for ((i=0; i<=outdirNum; i++)); do
		O_QSUB_NAME[$i]=${OUTDIR_NAME[$i]}/PROCESS_LOG_MULTITASK
		mkdir -p ${O_QSUB_NAME[$i]}
	done
	oqsubNum=$(( ${#O_QSUB_NAME[@]} - 1 ))
fi	

if  [[ -z $m_QSUB ]] ; then
	m_QSUB="ae"
fi

if  [[ -z $M_QSUB ]] ; then
	M_QSUB=$MAIL_CONF # defind in the .conf file
fi

if  [[ -z $t_QSUB ]] ; then
	t_QSUB="1"
elif [[ $t_QSUB != 1 ]] ; then 
	echo -e "\n### ERROR ###  -T OPTION DECLARED WITH CHARACTER STRINGS DIFFERENT FROM \"1\": $t_QSUB\n"
	usage
	exit 1
fi
echo -e "\nDATA NOT SPLITED BY CHROMOSOMES (-T OPTION SET TO MANDATORY VALUE $t_QSUB)\n"

if [[ ! -z $v_QSUB ]] ; then
	if [[$v_QSUB =~ FASTQ_READ1|BAM_FILE|$FASTQ_READ1_INI|FASTQ_READ2|$FASTQ_READ2_INI|OUTDIR|$OUTDIR|CONFIG_FILE|$CONFIG_FILE|RUNNING_OP|REF_GENOME|$REF_GENOME|REF_GENOME_TYPE|$REF_GENOME_TYPE|SAMPLE_NAME|$SAMPLE_NAME ]] ; then
		echo -e "\n### ERROR ###  VARIABLE(S) $v_QSUB DECLARED IN -V SHOULD BE OR ALREADY DECLARED IN -f, -r, -o, -c, -R -n -I, -B OR -K\n"
		usage
		exit 1
	fi
fi


################ END QSUB PARAMETERS


################ QSUB LOOP

for i in `seq 0  $read1Num` ; do
	SAMPLE_NAME_tempo=${SAMPLE_NAME[$i]}
	FASTQ_READ1_tempo=${FASTQ_READ1_NAME[$i]}
	if [[ ! -z $FASTQ_READ2_INI ]] ; then
		FASTQ_READ2_tempo=${FASTQ_READ2_NAME[$i]}
	fi
	N_QSUB_tempo="${N_QSUB[$i]}_process_log"
	if [[ -z $BED_FILE ]] ; then
		v_QSUB_tempo="SCRIPT_USED=$SCRIPT_USED,FASTQ_READ1=$FASTQ_READ1_tempo,CONFIG_FILE=$CONFIG_FILE,RUNNING_OP=$RUNNING_OP,REF_GENOME=$REF_GENOME,REF_GENOME_TYPE=$REF_GENOME_TYPE,REF_GENOME_USED=$REF_GENOME_USED,SAMPLE_NAME=$SAMPLE_NAME_tempo,REMOVE_TMP_BAM=$REMOVE_TMP_BAM"
	else
		v_QSUB_tempo="SCRIPT_USED=$SCRIPT_USED,FASTQ_READ1=$FASTQ_READ1_tempo,CONFIG_FILE=$CONFIG_FILE,RUNNING_OP=$RUNNING_OP,REF_GENOME=$REF_GENOME,REF_GENOME_TYPE=$REF_GENOME_TYPE,REF_GENOME_USED=$REF_GENOME_USED,BED_FILE=$BED_FILE,SAMPLE_NAME=$SAMPLE_NAME_tempo,REMOVE_TMP_BAM=$REMOVE_TMP_BAM"
	fi
	if [[ ! -z $FASTQ_READ2_INI ]] ; then
		v_QSUB_tempo="$v_QSUB_tempo,FASTQ_READ2=$FASTQ_READ2_tempo"
	fi
	if [[ ! -z $v_QSUB ]] ; then
		v_QSUB_tempo="$v_QSUB_tempo,$v_QSUB"
	fi
	if [[ ${#OUTDIR_NAME[@]} == 1 ]] ; then
		OUTDIR_NAME_tempo=${OUTDIR_NAME[0]}
	else # can only be the number of FASTQ_READ1
		OUTDIR_NAME_tempo=${OUTDIR_NAME[$i]}
	fi
	v_QSUB_tempo="$v_QSUB_tempo,OUTDIR=$OUTDIR_NAME_tempo"
	if [[ ${#O_QSUB_NAME[@]} == 1 ]] ; then
		o_QSUB_tempo=${O_QSUB_NAME[0]}
	else # can only be the number of FASTQ_READ1
		o_QSUB_tempo=${O_QSUB_NAME[$i]}
	fi
	BAM_PROC=`qsub -m $m_QSUB -l $l_QSUB -q $q_QSUB -j $j_QSUB -o $o_QSUB_tempo -M $M_QSUB -N $N_QSUB_tempo -t $t_QSUB  -v $v_QSUB_tempo  $FASTQ_PROCESS_FILE`
done


echo -e "\nALL CHECKS OK: PROCESS ENGAGED......................\n"
SCRIPT_USED=$(echo $SCRIPT_USED | sed 's/__/ /g') # replace 2 underscores by space in SCRIPT_USED
echo "SCRIPT_USED: $SCRIPT_USED" ;
echo "FASTQ_READ1_INI value: ${FASTQ_READ1_INI[@]}" ;
echo "FASTQ_READ1_NAME value: ${FASTQ_READ1_NAME[@]}" ;
echo "FASTQ_READ2_INI value: ${FASTQ_READ2_INI[@]}" ;
echo "FASTQ_READ2_NAME value: ${FASTQ_READ2_NAME[@]}" ;
echo "read1Num value: $read1Num"
echo "read2Num value: $read2Num"
echo "SAMPLE_NAME value: ${SAMPLE_NAME[@]}"
echo "OUTDIR value: $OUTDIR"
echo "OUTDIR_NAME value: ${OUTDIR_NAME[@]}"
echo "outdirNum value: $outdirNum"
echo "O_QSUB_NAME value: ${O_QSUB_NAME[@]}"
echo "oqsubNum value: $oqsubNum"
echo ""
echo "CONFIG_FILE value: $CONFIG_FILE"
echo "REF_GENOME_CONF value: $REF_GENOME_CONF"
echo ""
echo "FASTQ_PROCESS_FILE value: $FASTQ_PROCESS_FILE"
echo "\$l_QSUB: $l_QSUB"
echo "\$q_QSUB: $q_QSUB"
echo "\$j_QSUB: $j_QSUB"
echo "\$o_QSUB: $o_QSUB"
echo "\$m_QSUB: $m_QSUB"
echo "\$M_QSUB: $M_QSUB"
echo "\$N_QSUB: ${N_QSUB[@]}"
echo "\$t_QSUB: $t_QSUB"
echo "\$v_QSUB: $v_QSUB"
RUNNING_OP=$(echo $RUNNING_OP | sed 's/__/,/g') # replace 2 underscores by comma in SREF_GENOME_USED
echo "RUNNING_OP value: $RUNNING_OP" ;
echo "REF_GENOME value: $REF_GENOME" ;
echo "REF_GENOME_PATH value: $REF_GENOME_PATH" ;
echo "REF_GENOME_TYPE value: $REF_GENOME_TYPE" ;
REF_GENOME_USED=$(echo $REF_GENOME_USED | sed 's/__/ /g') # replace 2 underscores by space in SREF_GENOME_USED
echo "REF_GENOME_USED value: $REF_GENOME_USED" ;
echo "BED_FILE value: $BED_FILE" ;
echo "REMOVE_TMP_BAM value: $REMOVE_TMP_BAM" ;
echo ""
echo "SAMPLE_NAME_tempo value: $SAMPLE_NAME_tempo" ;
echo "FASTQ_READ1_tempo value: $FASTQ_READ1_tempo" ;
echo "FASTQ_READ2_tempo value: $FASTQ_READ2_tempo" ;
echo "FASTQ_READ1 value: $FASTQ_READ1" ;
echo "FASTQ_READ2 value: $FASTQ_READ2" ;
echo ""
echo "N_QSUB_tempo value: $N_QSUB_tempo" ;
echo "v_QSUB_tempo value: $v_QSUB_tempo" ;
echo "o_QSUB_tempo value: $o_QSUB_tempo" ;
echo "\$BAM_PROC: ${BAM_PROC[@]}"
echo ""







