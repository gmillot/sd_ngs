# Derived from BGI_ConsDiagRec_WG_workflow.conf by Leo Colmet-Daage
# This file allows to set parameters. A variable is set with values (or elements) and the export command creates the variable in the environment. For instance:  the variable ANNOVAR refers to the annovar_20130729 tool: ANNOVAR_CONF=/bioinfo/local/build/annovar_20130729

#!/bin/bash # shebang (#! https://en.wikipedia.org/wiki/Shebang_%28Unix%29) indicating to the shell what program to interpret the script with, when executed, probably optional here.
# export allow the variable to be use in subprocesses. Without export, the variable is only available in the current process. Example ANNOVAR_CONF=/bioinfo/local/build/annovar_20130729 instead of export ANNOVAR_CONF=/bioinfo/local/build/annovar_20130729

#_______________________________________________________________________________________________
# TOOLS
export FASTQC_CONF=/bioinfo/local/fastqc/fastqc
export BOWTIE2_CONF=/bioinfo/local/build/bowtie2-2.1.0/bowtie2
export BOWTIE2_BUILD_CONF=/bioinfo/local/build/bowtie2-2.1.0/bowtie2-build
export SAMTOOLS_CONF=/bioinfo/local/samtools # # dir path : samtools here is a directory (the samtools tool is inside the samtools directory)
export PICARD_CONF=/bioinfo/local/picard-tools
export GATK_CONF=/bioinfo/local/build/GATK/GenomeAnalysisTK-3.5/GenomeAnalysisTK.jar
export GeVAP_CONF=/bioinfo/guests/nbessolt/workspace/GeVAP
export BEDTOOLS_CONF=/bioinfo/local/BEDTools/bin
export FASTAFROMBED_CONF=/bioinfo/local/BEDTools/bin/fastaFromBed
export MUTECT_CONF=/bioinfo/local/build/muTect/mutect-1.1.7.jar
export R_CONF=/bioinfo/local/build/R/R-3.2.3/bin # dir path
export VARSCAN2_CONF=/bioinfo/local/build/VarScan/VarScan.v2.4.1.jar # local version in /bioinfo/guests/gmillot/Gael_code/VarScan.v2.3.9.jar
export ANNOVAR_CONF=/bioinfo/local/build/Annovar/annovar_20160201 # dir path

#_______________________________________________________________________________________________
# REFERENCE GENOME

export REF_GENOME_CONF=/data/annotations/Galaxy/Human/hg19/bowtie2/hg19.fa
export REF_GENOME_TYPE_CONF=hg19 # important ofr ANNOVAR as it probably seek the annotation files as $ANNOVAR_DB_CONF/${REF_GENOME_TYPE_CONF}_$ANNOTATION_CONF
# /data/annotations/Galaxy/Human/hg19/bowtie2/hg19.fa
# /data/annotations/Human/hg19/bowtie2_indexes/base/hg19.fa
# /data/annotations/Human/hg19/complete/chr1-22_XYM/hg19.chr1-22_XYM.fasta
# use this one for no index /data/annotations/Human/hg19/bowtie_index/hg19_base/hg19.fa

#_______________________________________________________________________________________________
# INPUT / OUTPUT
export EXT_CONF=30

#_______________________________________________________________________________________________
# PBS AND OTHER PARAMETERS
export MAIL_CONF="gael.millot@curie.fr"
export NB_NOD_CONF=1 # nod number (number of physical computer containing CPU)
export NB_THREADS_CONF=8 # CPU on the same nod. Be careful to know how many CPU per nod in bicalc.

#_______________________________________________________________________________________________
# BOWTIE2 PARAMETERS

export NB_MISMATCH_CONF=1

#_______________________________________________________________________________________________
# STAT
export LOD_CONF=5 #This term is equivalent to "significance" - i.e. is the improvement significant enough to merit realignment? Note that this number should be adjusted based on your particular data set. For low coverage and/or when looking for indels with low allele frequency, this number should be smaller (default=5)

#_______________________________________________________________________________________________
# BAM PROCESS PARAMETERS
export MIN_MAPQ_CONF=20 # minimal MAPQ to keep
export KNOWNSITES_BASERECALIBRATOR_CONF=/data/annotations/Human/dbsnp/dbsnp_137.hg19.vcf # to recalibrate reads

#_______________________________________________________________________________________________
# GATK COVERAGE PARAMETERS
export MIN_BASE_QUAL_CONF=10 # minimal base quality to keep
# MIN_MAPQ_CONF is used for minimal MAPQ to keep

#_______________________________________________________________________________________________
# VARSCAN2 PARAMETERS

export MIN_COV_CALL_CONF=1000 # Minimum read depth at a position to make a call [8]
export MIN_READ_CALL_CONF=4 # Minimum supporting reads at a position to call variants [2]
export MIN_QUAL_CALL_CONF=8 # Minimum base quality at a position to count a read [15]
export MIN_FREQ_CALL_CONF=0.0001 # Minimum variant allele frequency threshold [0.01]
export PVALUE_CALL_CONF=0.99 # Default p-value threshold for calling variants [99e-02]
export STRAND_FILTER_CALL_CONF=0  # 1 to apply a strand-bias filter

#_______________________________________________________________________________________________
# ANNOVAR PARAMETERS

export ANNOVAR_DB_CONF=/data/annotations/annovar/
export ANNOTATION_CONF=refGene,cosmic68,snp138,1000g2012apr_all,esp6500si_all,ljb23_all # separate files by comma
export ANNOTATION_OP_CONF=g,f,f,f,f,f # one of the three possibility g (gene), f (filter) or r (region) for each ANNOTATION_CONF in the same order
# refGene: define if the variant is in a gene, in UTR, etc...
# cosmic68: annotation of variants found in cancero (cosmic database version 68)
# snp138: annotation of variants found in cancero (db snp database version 138)
# esp6500si_all : database of variants in the popu. It is for polymorphisms. We can filter considering that a variant above 1% in the population is not pathogenic
# 1000g2012apr_all : 1000 genome variants, database of variants in the popu. . It is for polymorphisms
# ljb23_all: prediction of deleterious effects
# ANNOVAR probably seek the annotation files as $ANNOVAR_DB_CONF/${REF_GENOME_TYPE_CONF}_$ANNOTATION_CONF
# further information at : http://www.openbioinformatics.org/annovar/annovar_filter.html

#_______________________________________________________________________________________________
# R GRAPHICAL ANALYSIS


# INPUT
# OUPUT # voir si ca passe sans sinon with / at the end
export R_SOURCE_FILE1_CONF=/bioinfo/guests/gmillot/Gael_code/R_main_graphic_functions_20160924.R # Define the absolute pathway of the folder containing functions created by Gael Millot
export R_EXONS_COORD_CONF=/bioinfo/guests/gmillot/Gael_code/Exons_RB1_P06400_hg19.txt # Define the absolute pathway of the folder containing the input data files (must contain only .txt files containing the tables to  analyze)
# BED_TARGET
# sample.name
export R_TARGET_NAME_CONF="RB1" # write "ALL" for all the 
export R_TARGET_CHR_LOC_CONF="chr13"
export R_CHR_LENGTH_CONF=115169878 # hg19 ref genome https://www.ncbi.nlm.nih.gov/grc/human/data
export R_UPST_JUNC_LENGTH_CONF=20 # number of intronic bases flanking the 5' boundary of exons to consider 
export R_DOWNST_JUNC_LENGTH_CONF=10 # number of intronic bases flanking the 3' boundary of exons to consider 
export R_FLANK_CONF=200 # number of flanking base pair outside of each exon which set the boundaries of the coverage exon / target plot
export R_OPT_TXT_CONF="notxt" # optional text to add in graphics. Use "notxt" for no text
export R_FALSE_POS_CONF=/bioinfo/guests/gmillot/Gael_code/G75T02.hg19_multianno_freq1_forR.txt # optional file. Must be generated by the ctDNA script (ANNOT section). Write "noFPVfile" if nothing wanted (equivalent of empty)



# http://gatkforums.broadinstitute.org/wdl/discussion/1975/how-can-i-use-parallelism-to-make-gatk-tools-run-faster



