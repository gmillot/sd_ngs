#!/bin/bash

set -e # stop the workflow if a command return something different from 0 in $?. Use set -ex to have the blocking line. It is important to know that each instruction executed send a result in $? Either 0 (true) or something else (different from 0)

function show_time () {
    num=$1
    min=0
    hour=0
    day=0
    if((num>59));then
        ((sec=num%60))
        ((num=num/60))
        if((num>59));then
            ((min=num%60))
            ((num=num/60))
            if((num>23));then
                ((hour=num%24))
                ((day=num/24))
            else
                ((hour=num))
            fi
        else
            ((min=num))
        fi
    else
        ((sec=num))
    fi
    echo "$day"d "$hour"h "$min"m "$sec"s
}


STARTTIME=$(date)
echo -e "\n----PREPROCESS----\n"
echo -e "\nPROCESS IGNITION:" $STARTTIME "\n"
SCRIPT_USED=$(echo $SCRIPT_USED | sed 's/__/ /g') # replace 2 underscores by space in SCRIPT_USED
echo -e "\nSCRIPT USED: $SCRIPT_USED\n"

################ CHECK


if [[ ! -f $CONFIG_FILE ]]; then
	echo -e "\n--config_file option (config file) MISSING in job command (see workflow_fastq_gael.sh) OR config_for_NGS_workflow_gael.conf NOT PRESENT IN HOME DIRECTORY:"
	echo -e "  \$CONFIG_FILE: $CONFIG_FILE\n"
	exit 1
else
	source $CONFIG_FILE
fi

if [[ -z $BED_FILE ]]; then
	echo -e "\nNO BED FILE DECLARED\n"
else
	echo -e "\nBED_FILE DECLARED: \t$BED_FILE\n"
fi

if [[ ! -f $FASTQ_READ1 || ! -d $OUTDIR || -z $RUNNING_OP || ! -f $REF_GENOME || -z $SAMPLE_NAME ]]; then
	echo -e "\nAT LEAST ONE OF THE FOLLOWING FILES OR DIRECTORY PATH MISSING IN job command (see help of workflow_fastq_gael.sh) OR IN THE workflow_fastq_gael.sh CODE (check v_QSUB in the Worflow_gael.sh file):"
else
	echo -e "\nFOLLOWING REQUIRED FILES OR DIRECTORIES DETECTED:"
fi
echo -e "  \$FASTQ_READ1:\t$FASTQ_READ1"
echo -e "  \$OUTDIR:\t$OUTDIR"
echo -e "  \$RUNNING_OP:\t$RUNNING_OP"
echo -e "  \$REF_GENOME:\t$REF_GENOME"
echo -e "  \$SAMPLE_NAME:\t$SAMPLE_NAME"
if [[ ! -f $FASTQ_READ1 || ! -d $OUTDIR || -z $RUNNING_OP || ! -f $REF_GENOME || -z $SAMPLE_NAME ]]; then
	exit 1
fi

################ DIRECTORIES


TMP_QSUB=$OUTDIR/$SAMPLE_NAME/TMP_QSUB
DIR_STAT=$OUTDIR/$SAMPLE_NAME/STAT_INITIAL_BAM
DIR_QC=$OUTDIR/$SAMPLE_NAME/FASTQC
DIR_INI_BAM=$OUTDIR/$SAMPLE_NAME/INITIAL_BAM
DIR_MODIF_BAM=$OUTDIR/$SAMPLE_NAME/MODIF_BAM
DIR_TMP_BAM=$OUTDIR/$SAMPLE_NAME/TMP_BAM
DIR_GATK_COV=$OUTDIR/$SAMPLE_NAME/GATK_COV
DIR_VARSCAN=$OUTDIR/$SAMPLE_NAME/VARSCAN
if [[ $RUNNING_OP == annotation ]] ; then
	DIR_ANNOT=$OUTDIR
else
	DIR_ANNOT=$OUTDIR/$SAMPLE_NAME/ANNOT
fi
	if [[ $RUNNING_OP =~ fastqc|mapping ]] ; then
	mkdir -p $TMP_QSUB
	mkdir -p $DIR_STAT
	mkdir -p $DIR_QC
	mkdir -p $DIR_INI_BAM
	mkdir -p $DIR_MODIF_BAM
	mkdir -p $DIR_TMP_BAM
	mkdir -p $DIR_GATK_COV
	mkdir -p $DIR_VARSCAN
	mkdir -p $DIR_ANNOT
elif [[ $RUNNING_OP != annotation && (! -d $TMP_QSUB || ! -d $DIR_STAT || ! -d $DIR_QC || ! -d $DIR_INI_BAM || ! -d $DIR_MODIF_BAM || ! -d $DIR_TMP_BAM || ! -d $DIR_GATK_COV || ! -d $DIR_VARSCAN  || ! -d $DIR_ANNOT) ]]; then
	echo -e "\n-K ARGUMENT SELECTED IS $RUNNING_OP BUT AT LEAST ONE OF THE FOLLOWING DIRECTORY PATH MISSING :"
	echo -e "  \$TMP_QSUB:\t$TMP_QSUB"
	echo -e "  \$DIR_STAT:\t$DIR_STAT"
	echo -e "  \$DIR_QC:\t$DIR_QC"
	echo -e "  \$DIR_INI_BAM:\t$DIR_INI_BAM"
	echo -e "  \$DIR_MODIF_BAM:\t$DIR_MODIF_BAM"
	echo -e "  \$DIR_TMP_BAM:\t$DIR_TMP_BAM"
	echo -e "  \$DIR_GATK_COV:\t$DIR_GATK_COV"
	echo -e "  \$DIR_VARSCAN:\t$DIR_VARSCAN"
	echo -e "  \$DIR_ANNOT:\t$DIR_ANNOT"
	exit 1
elif [[ $RUNNING_OP != annotation ]] ; then
	echo -e "\n-K ARGUMENT SELECTED IS $RUNNING_OP AND FOLLOWING REQUIRED FILES OR DIRECTORIES DETECTED:"
	echo -e "  \$TMP_QSUB:\t$TMP_QSUB"
	echo -e "  \$DIR_STAT:\t$DIR_STAT"
	echo -e "  \$DIR_QC:\t$DIR_QC"
	echo -e "  \$DIR_INI_BAM:\t$DIR_INI_BAM"
	echo -e "  \$DIR_MODIF_BAM:\t$DIR_MODIF_BAM"
	echo -e "  \$DIR_TMP_BAM:\t$DIR_TMP_BAM"
	echo -e "  \$DIR_GATK_COV:\t$DIR_GATK_COV"
	echo -e "  \$DIR_VARSCAN:\t$DIR_VARSCAN"
	echo -e "  \$DIR_ANNOT:\t$DIR_ANNOT"
fi

################ END DIRECTORIES

################ CHECK


if [[ ! -f $FASTQ_READ2 ]] ; then
	echo -e "\nfastq REVERSE FILES DETECTED:"
	echo -e "  \$FASTQ_READ2:\t$FASTQ_READ2\n"
fi

REF_GENOME_USED=$(echo $REF_GENOME_USED | sed 's/__/ /g') # replace 2 underscores by space in SREF_GENOME_USED
RUNNING_OP=$(echo $RUNNING_OP | sed 's/__/,/g') # replace 2 underscores by comma in SREF_GENOME_USED
echo -e "\nREFERENCE GENOME USED: $REF_GENOME_USED\n"

if [[ ! -f $FASTQC_CONF || ! -f $BOWTIE2_CONF || ! -d $SAMTOOLS_CONF || ! -d $PICARD_CONF || ! -d $BEDTOOLS_CONF || ! -f $GATK_CONF || -z $LOD_CONF || ! -d $R_CONF || -z $NB_THREADS_CONF || -z $NB_MISMATCH_CONF || -z $MIN_MAPQ_CONF || ! -f $KNOWNSITES_BASERECALIBRATOR_CONF || -z $MIN_BASE_QUAL_CONF || ! -f $VARSCAN2_CONF || ! -d $ANNOVAR_CONF || -z $MIN_COV_CALL_CONF || -z $MIN_READ_CALL_CONF || -z $MIN_QUAL_CALL_CONF || -z $MIN_FREQ_CALL_CONF || -z $PVALUE_CALL_CONF || -z $STRAND_FILTER_CALL_CONF || -z $REF_GENOME_TYPE_CONF || ! -d $ANNOVAR_DB_CONF || -z $ANNOTATION_CONF || -z $ANNOTATION_OP_CONF ]]; then
	echo -e "\nAT LEAST ONE OF THE FOLLOWING FILES OR DIRECTORY PATH MISSING OR UNCORRECTLY DEFINED IN config_for_NGS_workflow_gael.conf:"
else
	echo -e "\nFOLLOWING REQUIRED FILES OR DIRECTORIES DETECTED IN THE config_for_NGS_workflow_gael.conf FILE:"
fi
echo -e "  \$FASTQC_CONF:\t$FASTQC_CONF"
echo -e "  \$BOWTIE2_CONF:\t$BOWTIE2_CONF"
echo -e "  \$SAMTOOLS_CONF:\t$SAMTOOLS_CONF"
echo -e "  \$PICARD_CONF:\t$PICARD_CONF"
echo -e "  \$BEDTOOLS_CONF:\t$BEDTOOLS_CONF"
echo -e "  \$GATK_CONF:\t$GATK_CONF"
echo -e "  \$LOD_CONF:\t$LOD_CONF"
echo -e "  \$R_CONF:\t$R_CONF"
echo -e "  \$NB_THREADS_CONF:\t$NB_THREADS_CONF"
echo -e "  \$NB_MISMATCH_CONF:\t$NB_MISMATCH_CONF"
echo -e "  \$MIN_MAPQ_CONF:\t$MIN_MAPQ_CONF"
echo -e "  \$KNOWNSITES_BASERECALIBRATOR_CONF:\t$KNOWNSITES_BASERECALIBRATOR_CONF"
echo -e "  \$MIN_BASE_QUAL_CONF:\t$MIN_BASE_QUAL_CONF"
echo -e "  \$VARSCAN2_CONF:\t$VARSCAN2_CONF"
echo -e "  \$ANNOVAR_CONF:\t$ANNOVAR_CONF"
echo -e "  \$MIN_COV_CALL_CONF:\t$MIN_COV_CALL_CONF"
echo -e "  \$MIN_READ_CALL_CONF:\t$MIN_READ_CALL_CONF"
echo -e "  \$MIN_QUAL_CALL_CONF:\t$MIN_QUAL_CALL_CONF"
echo -e "  \$MIN_FREQ_CALL_CONF:\t$MIN_FREQ_CALL_CONF"
echo -e "  \$PVALUE_CALL_CONF:\t$PVALUE_CALL_CONF"
echo -e "  \$STRAND_FILTER_CALL_CONF:\t$STRAND_FILTER_CALL_CONF"
echo -e "  \$REF_GENOME_TYPE_CONF:\t$REF_GENOME_TYPE_CONF"
echo -e "  \$ANNOVAR_DB_CONF:\t$ANNOVAR_DB_CONF"
echo -e "  \$ANNOTATION_CONF:\t$ANNOTATION_CONF"
echo -e "  \$ANNOTATION_OP_CONF:\t$ANNOTATION_OP_CONF\n"
if [[ ! -f $FASTQC_CONF || ! -f $BOWTIE2_CONF || ! -d $SAMTOOLS_CONF || ! -d $PICARD_CONF || ! -d $BEDTOOLS_CONF || ! -f $GATK_CONF || -z $LOD_CONF ||  ! -d $R_CONF || -z $NB_THREADS_CONF || -z $NB_MISMATCH_CONF || -z $MIN_MAPQ_CONF || ! -f $KNOWNSITES_BASERECALIBRATOR_CONF || -z $MIN_BASE_QUAL_CONF || ! -f $VARSCAN2_CONF || ! -d $ANNOVAR_CONF || -z $MIN_COV_CALL_CONF || -z $MIN_READ_CALL_CONF || -z $MIN_QUAL_CALL_CONF || -z $MIN_FREQ_CALL_CONF || -z $PVALUE_CALL_CONF || -z $STRAND_FILTER_CALL_CONF || -z $REF_GENOME_TYPE_CONF || ! -d $ANNOVAR_DB_CONF || -z $ANNOTATION_CONF || -z $ANNOTATION_OP_CONF ]]; then
	exit 1
fi

if [[ ! -f $R_SOURCE_FILE1_CONF || ! -f $R_EXONS_COORD_CONF || -z $R_TARGET_NAME_CONF || -z $R_TARGET_CHR_LOC_CONF || -z $R_CHR_LENGTH_CONF || -z $R_UPST_JUNC_LENGTH_CONF || -z $R_DOWNST_JUNC_LENGTH_CONF || -z $R_FLANK_CONF || -z $R_OPT_TXT_CONF || ! -f $R_FALSE_POS_CONF ]]; then
	echo -e "\nAT LEAST ONE OF THE FOLLOWING FILES OR DIRECTORY PATH MISSING OR UNCORRECTLY DEFINED IN THE R SECTION OF config_for_NGS_workflow_gael.conf:"
else
	echo -e "\nFOLLOWING REQUIRED FILES OR DIRECTORIES DETECTED IN THE R SECTION OF config_for_NGS_workflow_gael.conf FILE:"
fi
echo -e "  \$R_SOURCE_FILE1_CONF:\t$R_SOURCE_FILE1_CONF"
echo -e "  \$R_EXONS_COORD_CONF:\t$R_EXONS_COORD_CONF"
echo -e "  \$R_TARGET_NAME_CONF:\t$R_TARGET_NAME_CONF"
echo -e "  \$R_TARGET_CHR_LOC_CONF:\t$R_TARGET_CHR_LOC_CONF"
echo -e "  \$R_CHR_LENGTH_CONF:\t$R_CHR_LENGTH_CONF"
echo -e "  \$R_UPST_JUNC_LENGTH_CONF:\t$R_UPST_JUNC_LENGTH_CONF"
echo -e "  \$R_DOWNST_JUNC_LENGTH_CONF:\t$R_DOWNST_JUNC_LENGTH_CONF"
echo -e "  \$R_FLANK_CONF:\t$R_FLANK_CONF"
echo -e "  \$R_OPT_TXT_CONF:\t$R_OPT_TXT_CONF"
echo -e "  \$R_FALSE_POS_CONF:\t$R_FALSE_POS_CONF\n"
if [[ ! -f $R_SOURCE_FILE1_CONF || ! -f $R_EXONS_COORD_CONF || -z $R_TARGET_NAME_CONF || -z $R_TARGET_CHR_LOC_CONF || -z $R_CHR_LENGTH_CONF || -z $R_UPST_JUNC_LENGTH_CONF || -z $R_DOWNST_JUNC_LENGTH_CONF || -z $R_FLANK_CONF || -z $R_OPT_TXT_CONF || ! -f $R_FALSE_POS_CONF ]]; then
	exit 1
fi

echo -e "\n\$TMPDIR value (reserved variable of qsub) used as TMP file in different tools: $TMPDIR\n" ;

if [[ $RUNNING_OP =~ annotation && ! ( $ANNOTATION_OP_CONF =~ f|g|r ) ]] ; then
	echo -e "\n### ERROR ### THE ANNOTATION_OP_CONF DEFINED IN THE .conf FILE IS NOT MADE OF g, f, r ONLY: $ANNOTATION_OP_CONF\n"
	exit 1
fi
TEMPO1=( ${ANNOTATION_CONF//\,/ } )
annotation_conf_Num=$(( ${#TEMPO1[@]} )) 
TEMPO2=( ${ANNOTATION_OP_CONF//\,/ } )
annotation_op_conf_Num=$(( ${#TEMPO2[@]} )) 
if [[ $RUNNING_OP =~ annotation && ( $annotation_conf_Num -ne $annotation_op_conf_Num ) ]] ; then
	echo -e "\n### ERROR ### IN .conf FILE, THE NUMBER OF ITEM SEPARATED BY COMMA IN ANNOTATION_CONF ($annotation_conf_Num) != NUMBER OF ITEM SEPARATED BY COMMA IN ANNOTATION_OP_CONF ($annotation_op_conf_Num)\n"
	exit 1
fi



if [[ "${FASTQ_READ1: -6}" == ".fastq" && "${FASTQ_READ2: -6}" == ".fastq" ]] ; then 
	NB_READ_FASTQ1=$(cat $FASTQ_READ1 | wc -l | awk '{print $1/4}')
	NB_READ_FASTQ2=$(cat $FASTQ_READ2 | wc -l | awk '{print $1/4}')
	echo -e "\nNUMBER OF READS IN FASTQ1 (FORWARD): $NB_READ_FASTQ1"
	echo -e "NUMBER OF READS IN FASTQ2 (REVERSE): $NB_READ_FASTQ2"
	echo -e "SUM OF READS: $(( $NB_READ_FASTQ1 + $NB_READ_FASTQ2 ))\n"
elif [[ "${FASTQ_READ1: -3}" == ".gz" && "${FASTQ_READ2: -3}" == ".gz" ]] ; then 
	NB_READ_FASTQ1=$(zcat $FASTQ_READ1 | wc -l | awk '{print $1/4}')
	NB_READ_FASTQ2=$(zcat $FASTQ_READ2 | wc -l | awk '{print $1/4}')
	echo -e "\nNUMBER OF READS IN FASTQ1 (FORWARD): $NB_READ_FASTQ1"
	echo -e "NUMBER OF READS IN FASTQ2 (REVERSE): $NB_READ_FASTQ2"
	echo -e "SUM OF READS: $(( $NB_READ_FASTQ1 + $NB_READ_FASTQ2 ))\n"
elif [[ "${FASTQ_READ1: -4}" == ".bam" ]] ; then 
	NB_READ_BAM_INI=$(samtools view $FASTQ_READ1 | wc -l)
	echo -e "\nNUMBER OF READS IN THE INITIAL BAM FILE: $NB_READ_BAM_INI\n"
else
	echo -e "\nNUMBER OF READS NOT INDICATED BECAUSE AT LEAST ONE OF THE .fastq OR .bam FILE IS NOT ENDING BY .fastq OR .gz OR .bam\n"
fi

DIR_TEMPO=$(dirname "${FASTQ_READ1}")


################ END CHECK

################ FASTQC

if [[ $RUNNING_OP =~ fastqc ]]; then
	echo -e "\n----FASTQ QUALITY CONTROL OF $SAMPLE_NAME IN $DIR_QC/$SAMPLE_NAME SPECIFIED BY THE USER----\n"
	$FASTQC_CONF -o $DIR_QC -t $NB_THREADS_CONF -f fastq --quiet $FASTQ_READ1
	$FASTQC_CONF -o $DIR_QC -t $NB_THREADS_CONF -f fastq --quiet $FASTQ_READ2
	# FASTQC tool: https://biof-edu.colorado.edu/videos/dowell-short-read-class/day-4/fastqc-manual
	# https://wiki.gacrc.uga.edu/wiki/FastQC
	# to have the options:  /bioinfo/local/fastqc/fastqc -help
	# -o output dir
	# -t number of threads (defined in the .conf file)
	# -f format of input files (Bypasses the normal sequence file format detection)
	# --quiet Supress all progress messages on stdout and only report errors
else
	echo -e "\n----NO FASTQ QUALITY CONTROL OF $SAMPLE_NAME REQUIRED BY THE USER----\n"
fi


################ END FASTQC

################ MAPPING


if [[ $RUNNING_OP =~ mapping ]]; then
	echo -e "\n----ALIGNMENT SPECIFIED BY THE USER (BOWTIE2)----\n"
	$BOWTIE2_CONF -N $NB_MISMATCH_CONF -p $NB_THREADS_CONF -x ${REF_GENOME%.*} -1 $FASTQ_READ1 -2 $FASTQ_READ2 -S $DIR_INI_BAM/${SAMPLE_NAME}.sam
	# http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml
	# -N: number of mismatches authorized
	# -p: number of threads
	# -x: The basename of the index for the reference genome. The basename is the name of any of the index files up to but not including the final .1.bt2 / .rev.1.bt2 / etc.
	# -1: first fastq (forward)
	# -2: second fastq (reverse)
	# -S: File to write SAM alignments to
	# the bowtie2 output is piped in samtools to convert into .bam without creating .sam.
	# Beware: bowtie2 create directly the .sam file and fill it progressively
	# Beware the pipe toward samtools does not work: $BOWTIE2_CONF -N 1 -p $NB_THREADS_CONF -x $REF_GENOME -1 $FASTQ_READ1 -2 $FASTQ_READ2 -S $DIR_INI_BAM/$SAMPLE_NAME.sam | $SAMTOOLS_CONF/samtools view -Sb -o $DIR_INI_BAM/$SAMPLE_NAME.bam $DIR_INI_BAM/$SAMPLE_NAME.sam
	$SAMTOOLS_CONF/samtools view -Sbh -o $DIR_INI_BAM/${SAMPLE_NAME}.bam $DIR_INI_BAM/${SAMPLE_NAME}.sam
	$SAMTOOLS_CONF/samtools sort $DIR_INI_BAM/${SAMPLE_NAME}.bam $DIR_INI_BAM/$SAMPLE_NAME # usage is input the noutput without the suffix
	rm $DIR_INI_BAM/${SAMPLE_NAME}.sam
	$SAMTOOLS_CONF/samtools index $DIR_INI_BAM/${SAMPLE_NAME}.bam # create indexes
	TEMPO_VAR=$DIR_INI_BAM/${SAMPLE_NAME}.bam # I have to use this because DIR_TEMPO=$(dirname "${DIR_INI_BAM/${SAMPLE_NAME}.bam}") does not take the last directory
	DIR_TEMPO=$(dirname "${TEMPO_VAR}") # because the .bam file is in a different place depending on activation of mapping option or not
	NB_READ_BAM_INI=$(samtools view $TEMPO_VAR | wc -l) # Beware: take the same variable name than in check section
	echo -e "\nNUMBER OF READS IN THE INITIAL BAM FILE: $NB_READ_BAM_INI\n"
else
	echo -e "\n----NO MAPPING OF $SAMPLE_NAME REQUIRED BY THE USER----\n"
fi


################ END MAPPING

################ STAT


if [[ $RUNNING_OP =~ stats ]]; then
	echo -e "\n----STATISTICS ON ALIGNMENTS SPECIFIED BY THE USER----\n"
	
	#### Number of reads mapping on the reference genome ##
	NB=$($SAMTOOLS_CONF/samtools idxstats $DIR_TEMPO/${SAMPLE_NAME}.bam | awk '{MAPPED+= $3 ; UNMAPPED+=$4} END {print MAPPED+UNMAPPED,MAPPED,UNMAPPED}')

	### Number of reads mapping on the reference genome with a mapping quality above min_mapq ##
	$SAMTOOLS_CONF/samtools view -q $MIN_MAPQ_CONF -b $DIR_TEMPO/${SAMPLE_NAME}.bam > $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_stat.bam
	$SAMTOOLS_CONF/samtools index $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_stat.bam
	NB_READ_ABOVE_MIN_MAPQ=$($SAMTOOLS_CONF/samtools idxstats $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_stat.bam | awk '{SUM+= $3} END {print SUM}')
	# The output of idxstats is a table with each line consisting of (1) reference sequence name, (2) ref sequence length, (3) number of mapped reads and (4) number of unmapped reads
	# the last line is unmapped reads
	# About the fact that unmapped reads are on a chromo, see https://www.biostars.org/p/14569/ it says that SAM specs say that if the 4 flag is set, you can't believe chromosome, positions, CIGAR strings, mapping quality, or anything else in the .sam entry
	# here the code perform the sum of column 3 and print to be in NB_READ_ABOVE_MIN_MAPQ

	### Number of reads mapping with mapq > min_mapq on the target bed ##
	if [[ ! -z $BED_FILE ]]; then
		$BEDTOOLS_CONF/bedtools intersect -abam $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_stat.bam -b $BED_FILE >  $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_stat.bam
		$SAMTOOLS_CONF/samtools index $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_stat.bam
		NB_READ_ABOVE_MIN_MAPQ_ONTARGET=$($SAMTOOLS_CONF/samtools idxstats $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_stat.bam | awk '{SUM+= $3} END {print SUM}')

		### Number of reads mapping with mapq > min_mapq on the target bed without duplicates ##
		java -jar $PICARD_CONF/MarkDuplicates.jar I=$DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_stat.bam  O=$DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_nodup_stat.bam  M=$DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_nodup.metrics VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=TRUE REMOVE_DUPLICATES=TRUE VERBOSITY=WARNING QUIET=TRUE
		$SAMTOOLS_CONF/samtools index $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_nodup_stat.bam
		NB_READ_ABOVE_MIN_MAPQ_ONTARGET_NODUP=$($SAMTOOLS_CONF/samtools flagstat $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_nodup_stat.bam | awk 'NR==1 {print $1}')
		echo "Sample reads mapped unmapped mapped_q$MIN_MAPQ_CONF q${MIN_MAPQ_CONF}_onTarget q${MIN_MAPQ_CONF}_onTarget_nodup" > $DIR_STAT/${SAMPLE_NAME}.mapping.stats
		echo "${SAMPLE_NAME} $NB $NB_READ_ABOVE_MIN_MAPQ $NB_READ_ABOVE_MIN_MAPQ_ONTARGET $NB_READ_ABOVE_MIN_MAPQ_ONTARGET_NODUP " >> $DIR_STAT/${SAMPLE_NAME}.mapping.stats
		$BEDTOOLS_CONF/bedtools coverage -d -abam $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_nodup_stat.bam  -b $BED_FILE |awk '{h[$NF]++}; END { for(k in h) print k, h[k] }' | sort -V > $DIR_STAT/${SAMPLE_NAME}.cov.txt
	else
		NB_READ_ABOVE_MIN_MAPQ_ONTARGET="NA"
		NB_READ_ABOVE_MIN_MAPQ_ONTARGET_NODUP="NA"
		$BEDTOOLS_CONF/bedtools coverage -d -abam $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_stat.bam |awk '{h[$NF]++}; END { for(k in h) print k, h[k] }' | sort -V > $DIR_STAT/${SAMPLE_NAME}.cov.txt
	fi
	$R_CONF/Rscript /bioinfo/guests/gmillot/Gael_code/stats_Elodie_Girard.R $DIR_STAT
	if [[ $REMOVE_TMP_BAM -eq 0 ]]; then
		echo -e "\nREMOVE $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_stat.bam IN $DIR_TMP_BAM\n"
		rm $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_stat*
		if [[ ! -z $BED_FILE ]]; then
			echo -e "\nREMOVE $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_stat.bam"
			rm $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_stat*
			echo -e "REMOVE $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_nodup_stat.bam\n"
			rm $DIR_TMP_BAM/${SAMPLE_NAME}_q${MIN_MAPQ_CONF}_onTarget_nodup*
		fi
	fi
else
	echo -e "\n----NO STATISTICS ON ALIGNMENT SPECIFIED BY THE USER----\n"
fi


################ END STAT



################ MAPQ FILTERING


if [[ $RUNNING_OP =~ mapqfilter ]]; then
	echo -e "\n----MAPQ FILTERING OF BAM FILES REQUIRED BY THE USER----\n"
	#                            |--------------------------|
	#                              RemoveLowMAPQ20Reads_Samtools
	
	INPUT=$DIR_TEMPO/${SAMPLE_NAME}.bam
	OUTPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_Q${MIN_MAPQ_CONF}.bam
	
	# samtools view unzip the bam file (BGZF compression) which displays a sam, and allows parsing, execution in it (use the .bai file and goes fast)
	# http://www.htslib.org/doc/samtools.html
	# https://en.wikipedia.org/wiki/SAMtools
	# echo "\$TMP: $TMP" ; echo "$SAMTOOLS_CONF/samtools view  -H   $INPUT  > $TMP_QSUB/SamHeader_${SAMPLE_NAME}" # echo will be written in LOG 
	
	$SAMTOOLS_CONF/samtools view  -H   $INPUT  > $TMP_QSUB/SamHeader_${SAMPLE_NAME}
	# SAMTOOLS_CONF is a variable that contains a path (given by $SAMTOOLS_CONF) in .conf.
	# -H to output the header of the total bam file. Thus, Extract the header of the INPUT bam and store it in the SamHeader_${SAMPLE_NAME} variable created in the $TMP_QSUB directory
	# $SAMTOOLS_CONF defined in workflow_gael.conf (== /bioinfo/local/samtools)
	# $INPUT is the name of the intial bam processed ($INPUT = $RAWBAM = ${T_BAMs[$i]} = argument of -T in workflow_bam_gael.sh)
	# $TMP_QSUB is a temporary directory created by the workflow_bam_gael.sh, ie /bioinfo/guests/gmillot/job_result ($TMP_QSUB = $OUTDIR/TMP_QSUB with $OUTDIR = argument of -o in workflow_bam_gael.sh)
	
	$SAMTOOLS_CONF/samtools view -q $MIN_MAPQ_CONF $INPUT | cat $TMP_QSUB/SamHeader_${SAMPLE_NAME} - | $SAMTOOLS_CONF/samtools view -Sb -  > $OUTPUT
	# $SAMTOOLS_CONF/samtools view -h -q 20 -Sb $INPUT chr${chrom}  > $OUTPUT # we could have use this simple command. However, there is a bug with the header "[bam_header_read] EOF marker is absent."
	# -q 20 to recover the reads of mapq >= 20 (filter the read of low mapping quality) from the total bam but looking only in the required chromo ($INPUT chr${chrom} so the .bai is very important to find the chrom)
	# Then pipe (|) meaning redirection toward a file that start with the correct header (cat $TMP_QSUB/SamHeader_chr${chrom}), then - to say "add" what is before the pipe (samtools view -q 20 $INPUT chr${chrom}).
	# Then pipe toward a compressed file $SAMTOOLS_CONF/samtools view -Sb - (-Sb option to convert into bam and - to recover before the pipe, ie cat $TMP_QSUB/SamHeader_${SAMPLE_NAME} -) , which is put in the OUTPUT variable (> $OUTPUT)
	# rm   $TMP_QSUB/SamHeader_${SAMPLE_NAME} # Do not suppress the SamHeader_chr${chrom} file because is used by different process of qsub (split per chromo)
	if [[ $REMOVE_TMP_BAM -eq 0 ]]; then
		echo -e "\nREMOVE SamHeader_${SAMPLE_NAME} IN $TMP_QSUB\n"
		rm $TMP_QSUB/SamHeader_${SAMPLE_NAME}
	fi
	
	NB_READ_BAM_MAPQ=$(samtools view $OUTPUT | wc -l)
	echo -e "\nNUMBER OF READS IN THE BAM FILE AFTER MAPQ FILTERING: $NB_READ_BAM_MAPQ"
	if [[ ! -z $NB_READ_BAM_INI ]] ; then
		echo -e "READ LOSS: $(printf '%.2f\n' $(echo $" 100 - $NB_READ_BAM_MAPQ / $NB_READ_BAM_INI * 100 " | bc -l))%"
		echo -e "READ KEPT: $(printf '%.2f\n' $(echo $" $NB_READ_BAM_MAPQ / $NB_READ_BAM_INI * 100 " | bc -l))%\n"
	else
		echo -e "\n"
	fi
	#                            |--------------------------|
	#                             Reorder bam in karyotypic_Picard
	
	# http://broadinstitute.github.io/picard/command-line-overview.html
	# work on sam or bam files. Important to reorder the bam by position and by chromo, otherwise some tools like GATK crash. Here it is split by chromo, so reorder just by position
	# The most important function of ReorderSam is that it removes the reads that do not match the indicated ref: "Not to be confused with SortSam which sorts a SAM or BAM file with a valid sequence dictionary, ReorderSam reorders reads in a SAM/BAM file to match the contig ordering in a provided reference file, as determined by exact name matching of contigs. Reads mapped to contigs absent in the new reference are dropped"
	
	
	
	INPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_Q${MIN_MAPQ_CONF}.bam # this imput is exactly the output of the precedent section OUTPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_chr${chrom}_Q${MIN_MAPQ_CONF}.bam
	OUTPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_ReorderSam.bam
	
	# \[carriage return] is used to precise to the bash shell that there is no carriage return. This give cleaner code
	# Never put comments # behing the \ symbol (because this symbol tells to forget the carriage return, ie, everything should be written on a single line without \)
	# In fact, never put anything (space or other) between the \ symbol  and the carriage return
	# no need to create a .bai at this stage
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR	\
		  -jar $PICARD_CONF/ReorderSam.jar	\
		  I=$INPUT	\
		  O=$OUTPUT	\
		  REFERENCE=$REF_GENOME	\
		  VALIDATION_STRINGENCY=SILENT
	# No option for nb of threads
	# java -Xmx4g -Djava.io.tmpdir=$TMPDIR	\ # -Xmx4g to ask for 4Giga, -Djava.io.tmpdir tempo variable. the $TMPDIR directory is predefined by the cluster. All the intermediate files (log, etc.) from java are put in this directory. This directory will be systematically emptied at the end of each job.
	# -jar $PICARD_CONF/ReorderSam.jar	\   # - jar to call the java function to run. A .jar file is a non-lisible java executable. $PICARD_CONF is created in the .conf file (line 7). Reorder bam files (sam is the older designation before working on compressed files).
	# REFERENCE=$REF_GENOME	\  # DNA reference used.	
	# VALIDATION_STRINGENCY=SILENT	# inactivate the VALIDATION_STRINGENCY argument.  Setting stringency to SILENT can improve performance when processing a BAM file in which variable-length data (read, qualities, tags) do not otherwise need to be decoded. Write java and we will have java options (because argument are missing). Write java -jar /bioinfo/local/picard-tools/ReorderSam.jar -h to have the option of ReorderSam.jar
	
	if [[ $REMOVE_TMP_BAM -eq 0 ]]; then
		echo -e "\nREMOVE ${SAMPLE_NAME}_Q${MIN_MAPQ_CONF}.bam IN $DIR_TMP_BAM\n"
		rm $DIR_TMP_BAM/${SAMPLE_NAME}_Q${MIN_MAPQ_CONF}.bam
	fi
	
	#                            |--------------------------|
	#                                    Sort sam_Picard
	
	
	INPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_ReorderSam.bam
	OUTPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_sort.bam
	
	java -Xmx4g -Djava.io.tmpdir=$TMPDIR	\
	     -jar $PICARD_CONF/SortSam.jar			\
	     INPUT=$INPUT 						\
	     OUTPUT=$OUTPUT						\
	     CREATE_INDEX=true 					\
	     VALIDATION_STRINGENCY=SILENT	\
	     SO=coordinate
	# No option for nb of threads
	# -jar $PICARD_CONF/SortSam.jar			\  # SortSam
	# CREATE_INDEX=true 					\  # create .bai index
	# SO=coordinate # sort by chromo and by coordinates position (5' of each read) 	 
	if [[ $REMOVE_TMP_BAM -eq 0 ]]; then
		echo -e "\nREMOVE ${SAMPLE_NAME}_ReorderSam.bam IN $DIR_TMP_BAM\n"
		rm $DIR_TMP_BAM/${SAMPLE_NAME}_ReorderSam.bam
	fi
	
	echo -e "\nNUMBER OF READS IN THE BAM FILE AFTER REORDER AND SORT PROCESS: $(samtools view $OUTPUT | wc -l)\n"
	DIR_TEMPO=$(dirname "${OUTPUT}") # because the .bam file is in a different place depending on activation of mapping option or not
	SAMPLE_NAME_SUFFIX="_sort"
#	$SAMTOOLS_CONF/samtools index $DIR_TEMPO/${SAMPLE_NAME}${SAMPLE_NAME_SUFFIX}.bam # create indexes
else
	echo -e "\n----MAPQ FILTERING OF BAM FILES NOT REQUIRED BY THE USER----\n"
	SAMPLE_NAME_SUFFIX=""
fi

################ END MAPQ FILTERING



################ BAM READ GROUP MODIFICATION


if [[ $RUNNING_OP =~ reanalysis|coverage ]]; then
	echo -e "\n----READ GROUPS OF BAM FILE MODIFIED TO FIT GATK RECOMMENDATIONS----\n"
	
	# a read group is a kind of header that come from the same sample. A bam file is a unique file but can contain several samples (consist and tumoral). In such condition, important to separate these samples. 
	# Here the idea is to give the same read group to all the read of the same bam file (no sub categories in these files).
	# probably important for Alban Lermine, when converting fastq files to bam files with multiplexed experiments.
	
	INPUT=$DIR_TEMPO/${SAMPLE_NAME}${SAMPLE_NAME_SUFFIX}.bam
	OUTPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_rg.bam
	echo -e "\nINITIAL HEADER OF THE BAM FILE:\n\n$(samtools view -H $INPUT)\n"
	
	java -Xmx4g -Djava.io.tmpdir=$TMPDIR	\
		  -jar $PICARD_CONF/AddOrReplaceReadGroups.jar\
		  INPUT=$INPUT 						\
		  OUTPUT=$OUTPUT						\
		  RGID=$SAMPLE_NAME  					   \
		  RGLB=$SAMPLE_NAME  					   \
		  RGPL="illumina"  					\
		  RGPU="illumina"  					\
		  RGSM=$SAMPLE_NAME  					   \
		  SORT_ORDER=coordinate				\
		  VALIDATION_STRINGENCY=SILENT  	\
		  CREATE_INDEX=true 
	# No option for nb of threads
	# RGID=$SAMPLE_NAME  					   \  # Read Group ID
	# RGLB=$SAMPLE_NAME  					   \  # Read Group Library 
	# RGSM=$SAMPLE_NAME  					   \  # Read Group Sample Name
	# SORT_ORDER=coordinate				\  # idem Sort
	# VALIDATION_STRINGENCY=SILENT  	\  # idem Sort
	# CREATE_INDEX=true # idem Sort
	if [[ $REMOVE_TMP_BAM -eq 0 ]]; then
		if [[ -f $DIR_TMP_BAM/${SAMPLE_NAME}_sort.bam ]]; then
			echo -e "\nREMOVE ${SAMPLE_NAME}_sort.\{bam,bai\} IN $DIR_TMP_BAM\n"
			rm $DIR_TMP_BAM/${SAMPLE_NAME}_sort.ba*
		fi
	fi
	echo -e "\nNEW HEADER OF THE BAM FILE: $(samtools view -H $OUTPUT)\n"
	DIR_TEMPO=$(dirname "${OUTPUT}") # because the .bam file is in a different place depending on activation of mapping option or not
	SAMPLE_NAME_SUFFIX="_rg"
#	$SAMTOOLS_CONF/samtools index $DIR_TEMPO/${SAMPLE_NAME}${SAMPLE_NAME_SUFFIX}.bam # create indexes
else
	echo -e "\n----READ GROUPS OF BAM FILE NOT MODIFIED----\n"
fi



################ END BAM READ GROUP MODIFICATION




################ BAM REANALYSIS


if [[ $RUNNING_OP =~ reanalysis ]]; then
	echo -e "\n----REANALYSIS OF BAM FILES REQUIRED BY THE USER----\n"
	
	##                            |--------------------------|
	##                           Local realignment around indels_GATK
	
	# https://www.broadinstitute.org/gatk/guide/tooldocs/org_broadinstitute_gatk_tools_walkers_indels_RealignerTargetCreator.php
	# https://www.broadinstitute.org/gatk/guide/tooldocs/org_broadinstitute_gatk_tools_walkers_indels_IndelRealigner.php
	# create first a table of the indels detected. Two steps: RealignerTargetCreator and IndelRealigner
	
	INPUT=$DIR_TEMPO/${SAMPLE_NAME}${SAMPLE_NAME_SUFFIX}.bam
	OUTPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_realign.bam
	
	if [[ -z $BED_FILE ]] ; then
		#____step 1 : create a table of possible indels	
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \
			-T RealignerTargetCreator \
			-R $REF_GENOME \
			-I $INPUT \
			-o $INPUT.list \
			--known $KNOWNSITES_BASERECALIBRATOR_CONF\
			-nt $NB_THREADS_CONF \
		# java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \  # a bit different compared to Picard, since the GenomeAnalysisTK.jar global function is called and then commands are here called using -T.
		# -R $REF_GENOME  \  # DNA reference
		# -o $INPUT.list # Output with the name equal to $INPUT.list because it is a list		
	
		#____step 2 : realign read around this indels
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \
			-T IndelRealigner \
			-R $REF_GENOME \
			-I $INPUT \
			-o $OUTPUT \
			-known $KNOWNSITES_BASERECALIBRATOR_CONF \
			-targetIntervals $INPUT.list \
			-LOD $LOD_CONF \
		# -o $OUTPUT	\  # standard output name: ${SAMPLE_NAME}_realign.bam (gives a realigned bam file)
		# -targetIntervals $INPUT.list # list of indel used on the INPUT
	else
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \
			-T RealignerTargetCreator \
			-R $REF_GENOME \
			-I $INPUT \
			-o $INPUT.list \
			--known $KNOWNSITES_BASERECALIBRATOR_CONF \
			-nt $NB_THREADS_CONF \
			-L $BED_FILE
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \
			-T IndelRealigner \
			-R $REF_GENOME \
			-I $INPUT \
			-o $OUTPUT \
			-known $KNOWNSITES_BASERECALIBRATOR_CONF \
			-targetIntervals $INPUT.list \
			-LOD $LOD_CONF \
			-L $BED_FILE
		# -targetIntervals $INPUT.list # list of indel used on the INPUT
	fi
	if [[ $REMOVE_TMP_BAM -eq 0 ]]; then
		if [[ -f $DIR_TMP_BAM/${SAMPLE_NAME}_rg.bam ]]; then
			echo -e "\nREMOVE ${SAMPLE_NAME}_rg.\{bam,bai,bam.list\} IN $DIR_TMP_BAM\n"
			rm $DIR_TMP_BAM/${SAMPLE_NAME}_rg.ba* # this file has to be here because Read Group modified
		fi
	fi
	
	##                            |--------------------------|
	##                                 BaseRecalibrator_GATK
	
	# https://www.broadinstitute.org/gatk/guide/tooldocs/org_broadinstitute_gatk_tools_walkers_bqsr_BaseRecalibrator.php
	# https://www.broadinstitute.org/gatk/guide/tooldocs/org_broadinstitute_gatk_tools_walkers_readutils_PrintReads.php
	# realignemt around indels so some of the reads may have the mapq changed (one maq value per read)
	
	INPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_realign.bam # Beware: use this code when using RemoveDuplicates_Picard: INPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_chr${chrom}_noDup.bam
	OUTPUT_REP=$DIR_TMP_BAM/${SAMPLE_NAME}_recal_report.grp
	OUTPUT=$DIR_TMP_BAM/${SAMPLE_NAME}_recal.bam
	
	if [[ -z $BED_FILE ]] ; then
		# Count covariates
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \
			-T BaseRecalibrator \
			-R $REF_GENOME \
			-I $INPUT \
			-o $OUTPUT_REP \
			-knownSites $KNOWNSITES_BASERECALIBRATOR_CONF \
			-nct $NB_THREADS_CONF \
			-rf BadCigar \
		# -o $DIR_TMP_BAM/${SAMPLE_NAME}_chr${chrom}_recal_report.grp		\  # special output (kind of report).
		# -knownSites $KNOWNSITES_BASERECALIBRATOR_CONF \  # by default dbsnp database of the known snp (polymorphism), see the .conf file
		# -rf BadCigar # GATK does not like BadCigar. This allow to omit Bad Cigars
	
		# Table recalibration
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \
			-T PrintReads \
			-R $REF_GENOME \
			-I $INPUT \
			-o $OUTPUT \
			-BQSR $OUTPUT_REP \
			-nct $NB_THREADS_CONF \
			-rf BadCigar \
		# -T PrintReads									\  # recalibrate the bam file
		# -o $OUTPUT										\  # final clean bam file
		# -BQSR $DIR_TMP_BAM/${SAMPLE_NAME}_chr${chrom}_recal_report.grp		\  # -BQSR to use this table on the INPUT to recalibrate reads
	else
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \
			-T BaseRecalibrator \
			-R $REF_GENOME \
			-I $INPUT \
			-o $OUTPUT_REP \
			-knownSites $KNOWNSITES_BASERECALIBRATOR_CONF \
			-nct $NB_THREADS_CONF \
			-rf BadCigar \
			-L $BED_FILE
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \
			-T PrintReads \
			-R $REF_GENOME \
			-I $INPUT \
			-o $OUTPUT \
			-BQSR $OUTPUT_REP \
			-nct $NB_THREADS_CONF \
			-rf BadCigar \
			-L $BED_FILE
	fi
	if [[ $REMOVE_TMP_BAM -eq 0 ]] ; then
		echo -e "\nREMOVE ${SAMPLE_NAME}_realign.\{bam,bai\} IN $DIR_TMP_BAM\n"
		rm $DIR_TMP_BAM/${SAMPLE_NAME}_realign.ba*
	fi
	DIR_TEMPO=$(dirname "${OUTPUT}") # because the .bam file is in a different place depending on activation of mapping option or not
	SAMPLE_NAME_SUFFIX="_recal"
	echo -e "\nNUMBER OF READS IN THE PROCESSED BAM FILE: $(samtools view $DIR_TEMPO/${SAMPLE_NAME}_recal.bam | wc -l)"
else
	echo -e "\n----REANALYSIS OF BAM FILES NOT REQUIRED BY THE USER----\n"
fi

################ END BAM REANALYSIS


################ BAM FILE TRANSFERT

if [[ $RUNNING_OP =~ mapqfilter|reanalysis ]]; then
	OUTPUT=$DIR_MODIF_BAM/${SAMPLE_NAME}${SAMPLE_NAME_SUFFIX}.bam 
	mv $DIR_TEMPO/${SAMPLE_NAME}${SAMPLE_NAME_SUFFIX}.b* $DIR_MODIF_BAM
	echo -e "\nMOVE $DIR_TEMPO/${SAMPLE_NAME}${SAMPLE_NAME_SUFFIX}.b* FILES TO $DIR_MODIF_BAM DIRECTORY\n"
	DIR_TEMPO=$(dirname "${OUTPUT}")
fi

################ END BAM TRANSFERT


################ GATK COVERAGE



if [[ $RUNNING_OP =~ coverage ]]; then
	echo -e "\n----GATK COVERAGE OF BAM FILES REQUIRED BY THE USER----\n"


	# https://www.broadinstitute.org/gatk/guide/tooldocs/org_broadinstitute_gatk_tools_walkers_coverage_DepthOfCoverage.php

	INPUT=$DIR_TEMPO/${SAMPLE_NAME}${SAMPLE_NAME_SUFFIX}.bam
	OUTPUT=$DIR_GATK_COV/${SAMPLE_NAME}_gatk.cov

	if [[ -z $BED_FILE ]] ; then
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \
			-T DepthOfCoverage \
			-R $REF_GENOME \
			-I $INPUT \
			-o $OUTPUT \
			--omitIntervalStatistics \
			--includeDeletions \
			--minMappingQuality $MIN_MAPQ_CONF \
			--minBaseQuality $MIN_BASE_QUAL_CONF \
			-baseCounts  \
			-rf BadCigar
	else
		java -Xmx4g -Djava.io.tmpdir=$TMPDIR -jar $GATK_CONF \
			-T DepthOfCoverage \
			-R $REF_GENOME \
			-I $INPUT \
			-o $OUTPUT \
			-L $BED_FILE \
			--omitIntervalStatistics \
			--includeDeletions \
			--minMappingQuality $MIN_MAPQ_CONF \
			--minBaseQuality $MIN_BASE_QUAL_CONF \
			-baseCounts  \
			-rf BadCigar
	     # --omitIntervalStatistics \ # several things to avoid a too long computation. See below for --
	     # --includeDeletions \ # in the .cov created file, the last part of the base count is D (deletion, see README file.docx). This option to include the deletion in the counting. This means that when an internal deletion is present in the aligned read, 1 is counted for D at this ref position. Has to be checked: does the total depth of the .cov file include the D number with --includeDeletions and does not include D number without (consider 2 reads instead of one read with an internal deletion) ?
	     # --minMappingQuality 20 \ # security to be sure that only min mapq20 reads are taken for the depth count. 
	     # --minBaseQuality 10 \ # Also use the base quality from the fastq file which is present in bam files (IGV gives the base and read Quality score.
	     #-baseCounts  \ # give A T G C N D count and not only depth
	     # -rf BadCigar
	fi
	sed 's/:/\t/g ; s/ /\t/g' $OUTPUT > ${OUTPUT}_forR # replace the spaces and colons by a tab in the OUTPUT=$DIR_GATK_COV/${SAMPLE_NAME}_gatk.cov file and save it in a new file
	NEW_LINE="Chr\tPosition\tTotal_Depth\tAverage_Depth_sample\tDepth_for_${SAMPLE_NAME}\tA_base\tnb_A\tC_base\tnb_C\tG_base\tnb_G\tT_base\tnb_T\tN_base\tnb_N\tD_base\tnb_D"
	sed -i "1 s/.*/$(echo -e $NEW_LINE)/" ${OUTPUT}_forR # -i to replace the modification in the input file, 1 mean first line, we substitute everything (.*) by the $(echo -e $NEW_LINE) variable (the '"$(echo -e $NEW_LINE)"' can also be used to interpret the variable in the file if '' are used instead of "") # http://stackoverflow.com/questions/13438095/replace-1st-line-in-a-text-file-by-a-string-shell-scripting and http://unix.stackexchange.com/questions/69112/how-can-i-use-variables-when-doing-a-sed
	echo -e "\nTHE FIRST LINE OF $OUTPUT:\n$(echo ${OUTPUT} | head -1)\nHAS BEEN REPLACED IN ${OUTPUT}_forR BY:\n${NEW_LINE}\n"
else
	echo -e "\n----NO GATK COVERAGE OF BAM FILES REQUIRED BY THE USER----\n"
fi


################ END GATK COVERAGE


################ VARIANT CALLING


if [[ $RUNNING_OP =~ calling ]]; then
	echo -e "\n----VARSCAN2 VARIANT CALLING REQUIRED BY THE USER----\n"
	
	# http://www.htslib.org/doc/samtools.html
	
	INPUT=$DIR_TEMPO/${SAMPLE_NAME}${SAMPLE_NAME_SUFFIX}.bam
	OUTPUT_SAM=$DIR_VARSCAN/${SAMPLE_NAME}.mpileup
	OUTPUT=$DIR_VARSCAN/${SAMPLE_NAME}.vcf
	DIR_TEMPO=$(dirname "${OUTPUT}")
	if [[ ! -z $BED_FILE ]]; then
		$SAMTOOLS_CONF/samtools mpileup -A -B -d 1000000 -f $REF_GENOME -l $BED_FILE -Q $MIN_BASE_QUAL_CONF -q $MIN_MAPQ_CONF $INPUT > $OUTPUT_SAM
		# -A, --count-orphans	Do not skip anomalous read pairs in variant calling. 
		# -B, --no-BAQ	Disable probabilistic realignment for the computation of base alignment quality (BAQ). BAQ is the Phred-scaled probability of a read base being misaligned. Applying this option greatly helps to reduce false SNPs caused by misalignments. 
		# -d, --max-depth INT	At a position, read maximally INT reads per input file. Note that samtools has a minimum value of 8000/n where n is the number of input files given to mpileup. This means the default is highly likely to be increased. Once above the cross-sample minimum of 8000 the -d parameter will have an effect. [250] 
		# -f, --fasta-ref FILE	The faidx-indexed reference file in the FASTA format. The file can be optionally compressed by bgzip. [null] 
		# -l, --positions FILE	BED or position list file containing a list of regions or sites where pileup or BCF should be generated. Position list files contain two columns (chromosome and position) and start counting from 1. BED files contain at least 3 columns (chromosome, start and end position) and are 0-based half-open.
		# -q, -min-MQ INT	Minimum mapping quality for an alignment to be used [0] 
		# -Q, --min-BQ INT	Minimum base quality for a base to be considered [13] 
	else
		$SAMTOOLS_CONF/samtools mpileup -A -B -d 1000000 -f $REF_GENOME -Q $MIN_BASE_QUAL_CONF -q $MIN_MAPQ_CONF $INPUT > $OUTPUT_SAM
	fi
	
	# http://varscan.sourceforge.net/germline-calling.html#variant-quality-score
	java -Xmx10g -jar $VARSCAN2_CONF mpileup2cns $OUTPUT_SAM \
		--output-vcf 1 \
		--variants 1 \
		--min-coverage $MIN_COV_CALL_CONF \
		--min-reads2 $MIN_READ_CALL_CONF \
		--min-avg-qual $MIN_QUAL_CALL_CONF \
		--min-var-freq $MIN_FREQ_CALL_CONF \
		--p-value $PVALUE_CALL_CONF \
		--strand-filter $STRAND_FILTER_CALL_CONF > $OUTPUT
		# see config file for descriptions
	
	awk '!/^#/{print $0}' $OUTPUT > ${OUTPUT}_forR # remove the lines starting by # in the ${OUTPUT}_forR file created
	sed -i 's/%//g ; s/ADP=//g ; s/;WT=/\t/g ; s/;HET=/\t/g ; s/;HOM=/\t/g ; s/;NC=/\t/g ; s/GT:GQ:SDP:DP:RD:AD:FREQ:PVAL:RBQ:ABQ:RDF:RDR:ADF:ADR\t//g' ${OUTPUT}_forR
	sed -i 's/:/\t/g' ${OUTPUT}_forR # replace the colons by a tab in the OUTPUT=$DIR_GATK_COV/${SAMPLE_NAME}_gatk.cov file and save it in a new file
	# Add a clean freq using the four last column of the vcf file: RDF RDR ADF ADR
	awk 'BEGIN{OFS="\t"} {printf "%s\t%s\n", $0,($25+$26)/($23+$24+$25+$26)*100}' ${OUTPUT}_forR > ${OUTPUT}_forR2
	cat ${OUTPUT}_forR2 > ${OUTPUT}_forR
	rm ${OUTPUT}_forR2
	NEW_LINE2="CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tADP\tWT\tHET\tHOM\tNC\tGT\tGQ\tSDP\tDP\tRD\tAD\tFREQ\tPVAL\tRBQ\tABQ\tRDF\tRDR\tADF\tADR\tCLEAN_FREQ"
	sed -i "1 s/^/$(echo -e $NEW_LINE2)\n/" ${OUTPUT}_forR # add a first line into ${OUTPUT}_forR, do not forget \n for end line return in the file
	echo -e "\nTHE FIRST LINES OF $OUTPUT:\n$(awk '/^#/{print $0}' $OUTPUT)\nHAVE BEEN REPLACED IN ${OUTPUT}_forR BY:\n${NEW_LINE2}\n"
	echo -e "\nA NEW CLEAN_FREQ COLUMN HAS BEEN ADDED IN ${OUTPUT}_forR, WHICH CORRESPONDS TO COLUMNS (ADF+ADR)/(RDF+RDR+ADF+ADR)\nBUT BEWARE WITH VARSCAN VERSIONS: NUMBER OF COLUMNS ARE USED IN THE CODE, NOT THE COLUMNS NAMES"
	# Create files that filter according to the alternative frequency among the reads
	MUTATION_FREQ_THEO=("20" "10" "5" "1" "0.5" "0.1") # array
	echo -e "CLEAN_FREQ_CUTOFF\tLINE_NB" > ${OUTPUT}_summary_forR # create a new file for number of mutations above a frequency number
	for i in $(seq 0 $(( ${#MUTATION_FREQ_THEO[@]} - 1 )) ) ;  do
		awk -F '\t' -v var1=${MUTATION_FREQ_THEO[$i]} '$1 ~ /chr13/ && $27 >= var1 {print $0}' ${OUTPUT}_forR > ${OUTPUT}_freq${MUTATION_FREQ_THEO[$i]}_forR ;
		echo -e "${MUTATION_FREQ_THEO[$i]}\t$(cat ${OUTPUT}_freq${MUTATION_FREQ_THEO[$i]}_forR | wc -l)" >> ${OUTPUT}_summary_forR ;
		sed -i "1 s/^/$(echo -e $NEW_LINE2)\n/" ${OUTPUT}_freq${MUTATION_FREQ_THEO[$i]}_forR ; # add a header in the file
		# we can use the following R scrip instead of the three above lines but without creating the ${OUTPUT}_summary_forR file (see varscan2_analysis_Gael.R)
		# $R_CONF/Rscript /bioinfo/guests/gmillot/Gael_code/varscan2_analysis_Gael.R ${OUTPUT}_forR ${MUTATION_FREQ_THEO[$i]} # arg1: input file, arg2 cutoff value to compare # R script
	done
else
	echo -e "\n----NO VARSCAN2 VARIANT CALLING REQUIRED BY THE USER----\n"
fi

################ END VARIANT CALLING


################ ANNOTATION


if [[ $RUNNING_OP =~ annotation ]]; then
	echo -e "\n----ANNOVAR VARIANT ANNOTATION REQUIRED BY THE USER----\n"
	
	# tape: perl /bioinfo/local/build/Annovar/annovar_20160201/table_annovar.pl --help 
	# http://annovar.openbioinformatics.org/en/latest/
	# http://annovar.openbioinformatics.org/en/latest/user-guide/input/
	# http://annovar.openbioinformatics.org/en/latest/articles/VCF/
	
	INPUT=$DIR_TEMPO/${SAMPLE_NAME}.vcf
	OUTPUT_CONV=$DIR_ANNOT/${SAMPLE_NAME}.avinput
	OUTPUT=$DIR_ANNOT/${SAMPLE_NAME} #generate a multianno.txt file
	# ANNOTATIONS_OPERATION=$(echo $ANNOTATIONS | awk '{ORS=","; split($0,s,",") ; for (i=1;i<=length(s);i++) {if(s[i] == "refGene") {print "g"} else {print "f"}}}' | sed 's/,$//g')
	$ANNOVAR_CONF/convert2annovar.pl $INPUT -format vcf4 -comment -includeInfo -outfile $OUTPUT_CONV 
	# -format vcf4: convert to vcf4 format
	# -comment: header information
	# -includeInfo: give full info
	$ANNOVAR_CONF/table_annovar.pl $OUTPUT_CONV $ANNOVAR_DB_CONF -buildver $REF_GENOME_TYPE -protocol $ANNOTATION_CONF -operation $ANNOTATION_OP_CONF -nastring NA -outfile $OUTPUT --otherinfo -remove -thread $NB_THREADS_CONF
	# -buildver: genome built version. By default, ANNOVAR annotates variant on hg18 (human genome NCBI build 36) coordinate. Since the input file is in hg19 coordinate, we added -buildver hg19 in every command above. Similarly, if you generated variant calls from human GRCh38 coordinate, add -buildver hg38 in every command, if your variant file is from fly, add -buildver dm3 in every command that you use; if your variant file is from mouse, add -buildver mm9 in every command that you use
	# -protocol: comma-delimited string specifying annotation protocol. These strings typically represent database names in $ANNOVAR_DB_CONF
	# -operation:  tells ANNOVAR which operations to use for each of the protocols: g means gene-based, r means region-based and f means filter-based. 
	# -nastring: string to display when a score is not available
	# -outfile: prefix for the output file
	# --otherinfo: # add the supplemental columns, after the 5 first mandatory columns (chr stat stop ref alt) present in the INPUT col, into the OUTPUT file
	# -remove: remove all temporary files
	# -thread $NB_THREADS_CONF: nb of threads
	# # ANNOVAR probably seek the annotation files as $ANNOVAR_DB_CONF/${REF_GENOME_TYPE_CONF}_$ANNOTATION_CONF
	# gene.annotation function of annovar: requires only chr start stop. does not perform pathological annotation, just the position (inside gene or not, etc...)
	
	awk '!/^#/{print $0}' $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno.txt > $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR # remove the lines starting by # in the ${OUTPUT}_forR file created
	sed -i '1d' $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR # remove first line of a file
	sed -i 's/ /./g ; s/GT:GQ:SDP:DP:RD:AD:FREQ:PVAL:RBQ:ABQ:RDF:RDR:ADF:ADR\t//g' $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR # remove column 50 and replace space by a dot sed -i 's/[[:space:]]/./g' $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR
	awk -F '\t' 'BEGIN{OFS="\t"} {gsub(/:/, "\t", $50) ; gsub(/%/, "", $50) ; gsub(/ADP=/, "", $49) ; gsub(/;WT=/, "\t", $49) ; gsub(/;HET=/, "\t", $49) ; gsub(/;HOM=/, "\t", $49) ; gsub(/;NC=/, "\t", $49) ; print $0}' $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR > $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR2
	cat $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR2 > $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR
	awk 'BEGIN{OFS="\t"} {printf "%s\t%s\n", $0,($66+$67)/($64+$65+$66+$67)*100}' $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR > $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR2
	cat $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR2 > $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR
	rm $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR2
	NEW_LINE3="Chr\tStart\tEnd\tRef\tAlt\tFunc.refGene\tGene.refGene\tGeneDetail.refGene\tExonicFunc.refGene\tAAChange.refGene\tcosmic68\tsnp138\t1000g2012apr_all\tesp6500si_all\tLJB23_SIFT_score\tLJB23_SIFT_score_converted\tLJB23_SIFT_pred\tLJB23_Polyphen2_HDIV_score\tLJB23_Polyphen2_HDIV_pred\tLJB23_Polyphen2_HVAR_score\tLJB23_Polyphen2_HVAR_pred\tLJB23_LRT_score\tLJB23_LRT_score_converted\tLJB23_LRT_pred\tLJB23_MutationTaster_score\tLJB23_MutationTaster_score_converted\tLJB23_MutationTaster_pred\tLJB23_MutationAssessor_score\tLJB23_MutationAssessor_score_converted\tLJB23_MutationAssessor_pred\tLJB23_FATHMM_score\tLJB23_FATHMM_score_converted\tLJB23_FATHMM_pred\tLJB23_RadialSVM_score\tLJB23_RadialSVM_score_converted\tLJB23_RadialSVM_pred\tLJB23_LR_score\tLJB23_LR_pred\tLJB23_GERP++\tLJB23_PhyloP\tLJB23_SiPhy\tCHROM_varscan\tPOS_varscan\tID_varscan\tREF_varscan\tALT_varscan\tQUAL_varscan\tFILTER_varscan\tADP\tWT\tHET\tHOM\tNC\tGT\tGQ\tSDP\tDP\tRD\tAD\tFREQ\tPVAL\tRBQ\tABQ\tRDF\tRDR\tADF\tADR\tCLEAN_FREQ"
	sed -i "1 s/^/$(echo -e $NEW_LINE3)\n/" $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR # add a first line into ${OUTPUT}_forR, do not forget \n for end line return in the file
	echo -e "\nTHE FIRST LINES OF $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno.txt:\n$(sed -n 1p $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno.txt)\n$(awk '/^#/{print $0}' $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno.txt)\nHAVE BEEN REPLACED IN $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR BY:\n${NEW_LINE3}\n"
	echo -e "\nA NEW CLEAN_FREQ COLUMN HAS BEEN ADDED IN $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR, WHICH CORRESPONDS TO COLUMNS (ADF+ADR)/(RDF+RDR+ADF+ADR)\nBUT BEWARE WITH ANNOVAR VERSIONS: NUMBER OF COLUMNS ARE USED IN THE CODE, NOT THE COLUMNS NAMES"
	# Create files that filter according to the alternative frequency among the reads
	MUTATION_FREQ_THEO=("20" "10" "5" "1" "0.5" "0.1") # array
	echo -e "CLEAN_FREQ_CUTOFF\tLINE_NB" > ${OUTPUT}_summary_forR # create a new file for number of mutations above a frequency number
	for i in $(seq 0 $(( ${#MUTATION_FREQ_THEO[@]} - 1 )) ) ;  do
		awk -F '\t' -v var1=${MUTATION_FREQ_THEO[$i]} '$1 ~ /chr13/ && $68 >= var1 {print $0}' $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR > $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_freq${MUTATION_FREQ_THEO[$i]}_forR ;
		echo -e "${MUTATION_FREQ_THEO[$i]}\t$(cat $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_freq${MUTATION_FREQ_THEO[$i]}_forR | wc -l)" >> ${OUTPUT}_summary_forR ;
		sed -i "1 s/^/$(echo -e $NEW_LINE3)\n/" $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_freq${MUTATION_FREQ_THEO[$i]}_forR ; # add a header in the file
		# we can use the following R scrip instead of the three above lines but without creating the ${OUTPUT}_summary_forR file (see annot_analysis_Gael.R)
		# $R_CONF/Rscript /bioinfo/guests/gmillot/Gael_code/annot_analysis_Gael.R $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR ${MUTATION_FREQ_THEO[$i]} # arg1: input file, arg2 cutoff value to compare
	done
	if [[ ! -z $BED_FILE ]]; then
		$R_CONF/Rscript /bioinfo/guests/gmillot/Gael_code/Freq_graphic_display.R $DIR_ANNOT/${SAMPLE_NAME}.hg19_multianno_forR $DIR_ANNOT $R_SOURCE_FILE1_CONF $R_EXONS_COORD_CONF $BED_FILE $SAMPLE_NAME $R_TARGET_NAME_CONF $R_TARGET_CHR_LOC_CONF $R_CHR_LENGTH_CONF $R_UPST_JUNC_LENGTH_CONF $R_DOWNST_JUNC_LENGTH_CONF $R_FLANK_CONF $R_OPT_TXT_CONF $R_FALSE_POS_CONF
	else
		echo -e "\nNO GRAPHICAL ANALYSES (REQUIRE A BED FILE WHICH HAS NOT BEEN PROVIDED\n"
	fi
else
	echo -e "\n----NO ANNOVAR VARIANT ANNOTATION REQUIRED BY THE USER----\n"
fi

################ END ANNOTATION



echo -e "\n----POSTPROCESS----\n"
STOPTIME=$(date)
echo -e "PROCESS END:" $STOPTIME
echo -e "JOB LAPSE TIME: " $(show_time $(($(date -d"$STOPTIME" +%s) - $(date -d"$STARTTIME" +%s))))


