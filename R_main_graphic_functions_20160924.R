################################################################
##                                                            ##
##     MAIN FUNCTIONS                                         ##
##                                                            ##
##     Gael A. Millot                                         ##
##     Version v1                                             ##
##     Compatible with R v3.2.2                               ##
##                                                            ##
################################################################


# BEWARE: do not forget to save the modifications also as .txt file and .R file


################ Exporting results (text & tables)


fun.export.data <- function(data, output, path = path3, no.overwrite = TRUE, rownames.table = FALSE, sep = 2){
  # data: object to print in the .txt. file
  # output: name of the output file
  # path: location of the output file
  # overwrite (logical) defines if the output file content has to be erased or not (default FALSE)
  # rownames.table (logical) defines whether row names have to be removed or not in small tables (less than length.rows rows)
  # sep: number of separating lines after printed data (must be integer)
  rownames.output.tables <- ""
  length.rows <- 4
  for(i in 1:length.rows){ # suppress the rownames of the first 4 rows. This method cannot be extended to more rows as the printed table is shifted on the right because of "big empty rownames"
    rownames.output.tables <- c(rownames.output.tables, paste0(rownames.output.tables[i]," ", collapse=""))
  }
  if(all(class(data) %in% c("table", "matrix"))){
    capture.output(data, file=paste0(path, "/", output), append = no.overwrite)
  }else{
    if(is.vector(data) & class(data) != "list" & length(data) == 1){
      cat(data, file= paste0(path, "/", output), append = no.overwrite)
    }else{
      if((class(data) == "data.frame" | class(data) == "matrix" | class(data) == "table") & rownames.table == FALSE){
        if(nrow(data) <= length.rows & nrow(data) != 0){
          row.names(data) <- rownames.output.tables[1:nrow(data)]
        }
      }
      capture.output(data, file=paste0(path, "/", output), append = no.overwrite)
    }
  }
  sep.final <- paste0(rep("\n", sep), collapse = "")
  write(sep.final, file= paste0(path, "/", output), append = TRUE)
}


################ Graphic window width and height + plot/graphic margins


# This is the order to use (all are mandatory functions):
# fun.window.size()
# fun.open.window()
# fun.graph.param() or fun.graph.param.prior.axis()
# plot()
# fun.axis.bg.feature() if fun.graph.param.prior.axis() has been used
# fun.close.pdf()


fun.window.size <- function(categ = NULL, down.space = 7, left.space = 7, up.space = 5, right.space = 2, inches.per.nb.f = 1){
  # categ: number of classes (levels in Name column of the dataframe to analyze. Has to be executed after fun.graph.param()
  # up.space: upper vertical margin between plot region and grapical window defined by par(mar)
  # down.space: lower vertical margin
  # left.space: left horizontal margin
  # right.space: right horizontal margin
  # inches.per.nb.f: number of inches per unit of nb.mut.graph. 2 means 2 inches for each boxplot for instance
  down.space <<- down.space
  left.space <<- left.space
  up.space <<- up.space
  right.space <<- right.space
  nb.mut.graph <<- length(categ)
  range.max <<- nb.mut.graph + 0.5 # the max range in the plot
  range.min <<- 0.5
  inches.sides <<- (left.space + right.space) * 0.2 # this is the size in inches occupied by the vertical margins of the graph. Indeed, par()$mai/par()$mar is generally equal to 0.2 inches per line, with par()$mai the margins in inches and par()$mar the margins in lines
  width.wind.var <<- inches.sides + inches.per.nb.f * (range.max - range.min)
}


################ Graphs in R or pdf


fun.open.window <- function(pdf.disp = c(activate.pdf, activate.pdf.tests), path.fun = path3, pdf.name.file = "graphs", width.fun = 7, height.fun = 7){
  # pdf.disp: use pdf or not
  # path.fun: where the pdf is saved. Must not finish by a path separator
  # pdf.name.file: name of the pdf file containing the graphs
  # width.fun: width of the windows (in inches). Can be width.wind.var from fun.window.size function
  # height.fun: height of the windows (in inches)
  if(pdf.disp == TRUE){
    pdf.loc<<-paste0(path.fun, "/", pdf.name.file, ".pdf")
    pdf(width = width.fun, height = height.fun, file=pdf.loc)
  }else{
    windows(width = width.fun, height = height.fun, rescale="fixed")
  }
  tempo.par.ini <<- par(no.readonly = TRUE) # to recover the initial parameters for next figure reion when device region split into several figure regions
}


################ Graphical parameters


fun.graph.param <- function(down.space.fun = down.space, left.space.fun = left.space, up.space.fun = up.space, right.space.fun = right.space, orient = 1, dist.legend = 4.5, tick.length = 0.5, box.type = "l", amplif.label = amplif, amplif.axis = amplif){
  ## BEWARE: require fun.window.size() function
  ## fun.window.size() function: down.space
  ## fun.window.size() function: left.space
  ## fun.window.size() function: up.space
  ## fun.window.size() function: right.space
  # up.space.fun: upper vertical margin between plot region and grapical window defined by par(mar)
  # down.space.fun: lower vertical margin
  # left.space.fun: left horizontal margin
  # right.space.fun: right horizontal margin
  # orient: for par(las)
  # dist.legend: increase the number to move axis legends away
  # tick.length: length of the ticks (1 means complete the distance between the plot region and the axis numbers, 0.5 means half the length, etc.
  # box.type: equivalent to par()$bty
  # amplif.label: increase or decrease the size of the text in legends
  # amplif.axis: increase or decrease the size of the numbers in axis
  par(mar = c(down.space.fun, left.space.fun, up.space.fun, right.space.fun), las = orient, mgp = c(dist.legend, 1, 0), xpd = FALSE, bty= box.type, cex.lab = amplif.label, cex.axis = amplif.axis)
  par(tcl = -par()$mgp[2] * tick.length) # tcl gives the length of the ticks as proportion of line text, knowing that mgp is in text lines. So the main ticks are a 0.5 of the distance of the axis numbers by default. The sign provides the side of the tick (negative for outside of the plot region)
}


fun.graph.param.prior.axis <- function(xlog.scale = "", ylog.scale = "", ann.fun = FALSE, xaxt.fun = "n", yaxt.fun = "n", down.space.fun = down.space, left.space.fun = left.space, up.space.fun = up.space, right.space.fun = right.space, orient = 1, dist.legend = 4.5, tick.length = 0.5, box.type = "n", amplif.label = amplif, amplif.axis = amplif){
  ## BEWARE: require fun.window.size() function
  ## fun.window.size() function: down.space
  ## fun.window.size() function: left.space
  ## fun.window.size() function: up.space
  ## fun.window.size() function: right.space
  # xlog.scale: if there is a log scale planned or not for x-axis. Use xlog.scale or other
  # ylog.scale: if there is a log scale planned or not for y-axis. Use ylog.scale or other
  # ann.fun: remove labels of axis (TRUE OR FALSE ONLY)
  # xaxt.fun: remove x-axis except labels ("n" or "s" only). Inactivated if xlog.scale = "x"
  # yaxt.fun: remove y-axis except labels ("n" or "s" only). Inactivated if ylog.scale = "y"
  # up.space.fun: upper vertical margin between plot region and grapical window defined by par(mar)
  # down.space.fun: lower vertical margin
  # left.space.fun: left horizontal margin
  # right.space.fun: right horizontal margin
  # orient: for par(las)
  # dist.legend: increase the number to move axis legends away
  # tick.length: length of the ticks (1 means complete the distance between the plot region and the axis numbers, 0.5 means half the length, etc.
  # box.type: equivalent to par()$bty
  # amplif.label: increase or decrease the size of the text in legends
  # amplif.axis: increase or decrease the size of the numbers in axis
  par(mar = c(down.space.fun, left.space.fun, up.space.fun, right.space.fun), ann = ann.fun, xaxt = xaxt.fun, yaxt = yaxt.fun , las = orient, mgp = c(dist.legend, 1, 0), xpd = FALSE, bty= box.type, cex.lab = amplif.label, cex.axis = amplif.axis)
  par(tcl = -par()$mgp[2] * tick.length) # tcl gives the length of the ticks as proportion of line text, knowing that mgp is in text lines. So the main ticks are a 2/3 of the distance of the axis numbers by default. The sign provides the side of the tick (negative for outside of the plot region)
  if(xlog.scale == "x"){
    par(xaxt = "n", xlog = TRUE) # suppress the x-axis label
  }else{
    par(xlog = FALSE)
  }
  if(ylog.scale == "y"){
    par(yaxt = "n", ylog = TRUE) # suppress the y-axis label
  }else{
    par(ylog = FALSE)
  }
}


################ Graphical features


fun.axis.bg.feature <- function(x.side.fun = "", x.lab.fun = "", x.dist.legend = 4.5, x.log.scale = "", x.nb.inter.tick = 1, y.side.fun = 2, y.lab.fun = "", y.dist.legend = 4.5, y.log.scale = "", y.nb.inter.tick = 1, tick.length = 0.5, sec.tick.length = 0.3, bg.color = NULL, grid.lwd = NULL, grid.col = "white", text.corner = "", magnific = amplif, magnific.text.corner = amplif/1.5, par.ini.fun = FALSE){
  ## Redesign axis. If x.side.fun = "", x.lab.fun = "", just add text at topright of the graph and reset par()
  ## BEWARE: require fun.graph.param() or fun.graph.param.prior.axis() because of tempo.par.ini
  # redefine the axis of the plot & the background of the plot region
  # x.side.fun: axis at the bottom (1) or top (3) of the region figure. Write "" for no change
  # x.lab.fun: label of the x-axis
  # x.dist.legend: increase the number to move x-axis legends away
  # x.log.scale: if there is a log scale planned or not for x-axis. Use log.scale or other
  # x.nb.inter.tick: number of secondary ticks between main ticks on x-axis (only if not log scale). -1 means non secondary ticks
  # y.side.fun: axis at the left (2) or right (4) of the region figure. Write "" for no change
  # y.lab.fun: label of the y-axis
  # y.dist.legend: increase the number to move y-axis legends away
  # y.log.scale: if there is a log scale planned or not for y-axis. Use log.scale or other
  # y.nb.inter.tick: number of secondary ticks between main ticks on y-axis (only if not log scale). -1 means non secondary ticks
  # tick.length: length of the main ticks (1 means complete the distance between the plot region and the axis numbers, 0.5 means half the length, etc.
  # sec.tick.length: length of the secondary ticks (1 means complete the distance between the plot region and the axis numbers, 0.5 means half the length, etc.
  # bg.color: background color of the plot region. NULL for no color. BEWARE: recover an existing plot !
  # grid.lwd: if non NULL, active the grid line (specify the line width)
  # grid.col: grid line color (only if grid.lwd non NULL)
  # text.corner: text to add at the top right corner of the window
  # magnific: increase or decrease the value to increase or decrease the size of the axis (numbers and legends)
  # magnific.text.corner: increase or decrease the size of the text
  # par.ini.fun: to reset all the graphics parameters. BEWARE: TRUE can generates bugs, mainly in graphic devices with multiple figure regions
  par(tcl = -par()$mgp[2] * tick.length)
  if(x.log.scale == "x"){
    grid.coord.x <- c(10^par("usr")[1], 10^par("usr")[2])
  }else{
    grid.coord.x <- c(par("usr")[1], par("usr")[2])
  }
  if(y.log.scale == "y"){
    grid.coord.y <- c(10^par("usr")[3], 10^par("usr")[4])
  }else{
    grid.coord.y <- c(par("usr")[3], par("usr")[4])
  }
  if( ! is.null(bg.color)){
    rect(grid.coord.x[1], grid.coord.y[1], grid.coord.x[2], grid.coord.y[2], col = bg.color, border = NA)
  }
  if( ! is.null(grid.lwd)){
    grid(nx = NA, ny = NULL, col = grid.col, lty = 1, lwd = grid.lwd)
  }
  if(x.side.fun == 1 | x.side.fun == 3){
    par(xpd=FALSE, xaxt="s")
    if(x.log.scale == "x"){
      if(any(par()$xaxp[1:2] == 0)){
        if(par()$xaxp[1] == 0){
          par(xaxp = c(10^-30, par()$xaxp[2:3])) # because log10(par()$xaxp[1] == 0) == -Inf
        }
        if(par()$xaxp[2] == 0){
          par(xaxp = c(par()$xaxp[1], 10^-30, par()$xaxp[3])) # because log10(par()$xaxp[2] == 0) == -Inf
        }
      }
      axis(side=x.side.fun, at=c(10^par()$usr[1], 10^par()$usr[2]), labels=rep("", 2), lwd=1, lwd.ticks=0) # draw the axis line
      mtext(side = x.side.fun, text = x.lab.fun, line = x.dist.legend, las = 0)
      par(tcl = -par()$mgp[2] * sec.tick.length) # lenght of the secondary ticks are reduced
      suppressWarnings(rug(10^outer(c((log10(par("xaxp")[1]) -1):log10(par("xaxp")[2])), log10(1:10), "+"), ticksize = NA, side = x.side.fun)) # ticksize = NA to allow the use of par()$tcl value
      par(tcl = -par()$mgp[2] * tick.length) # back to main ticks
      axis(side = x.side.fun, at = c(1e-15, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9, 1e10), labels = expression(10^-15, 10^-14, 10^-13, 10^-12, 10^-11, 10^-10, 10^-9, 10^-8, 10^-7, 10^-6, 10^-5, 10^-4, 10^-3, 10^-2, 10^-1, 10^0, 10^1, 10^2, 10^3, 10^4, 10^5, 10^6, 10^7, 10^8, 10^9, 10^10), lwd = 0, lwd.ticks = 1, cex.axis = magnific)
      x.text <- 10^par("usr")[2]
    }else{
      axis(side=x.side.fun, at=c(par()$usr[1], par()$usr[2]), labels=rep("", 2), lwd=1, lwd.ticks=0) # draw the axis line
      axis(side=x.side.fun, at=round(seq(par()$xaxp[1], par()$xaxp[2], length.out=par()$xaxp[3]+1), 2))
      mtext(side = x.side.fun, text = x.lab.fun, line = x.dist.legend, las = 0)
      if(x.nb.inter.tick > 0){
        inter.tick.unit <- (par("xaxp")[2] - par("xaxp")[1]) / par("xaxp")[3]
        par(tcl = -par()$mgp[2] * sec.tick.length) # lenght of the ticks are reduced
        suppressWarnings(rug(seq(par("xaxp")[1] - 10 * inter.tick.unit, par("xaxp")[2] + 10 * inter.tick.unit, by = inter.tick.unit / (1 + x.nb.inter.tick)), ticksize = NA, x.side.fun)) # ticksize = NA to allow the use of par()$tcl value
        par(tcl = -par()$mgp[2] * tick.length) # back to main ticks
      }
      x.text <- par("usr")[2]
    }
  }else{
    x.text <- par("usr")[2]
  }
  if(y.side.fun == 2 | y.side.fun == 4){
    par(xpd=FALSE, yaxt="s")
    if(y.log.scale == "y"){
      mid.bottom.space <<- 10^(par("usr")[3] - ((par("usr")[4] -  par("usr")[3]) / (par("plt")[4] - par("plt")[3])) * par("plt")[3] / 2) # to position axis labeling at the bottom of the graph (according to y scale)
      mid.top.space <<- 10^(par("usr")[4] + ((par("usr")[4] -  par("usr")[3]) / (par("plt")[4] - par("plt")[3])) * par("plt")[3] / 2) # to position axis labeling at the top of the graph (according to y scale)
      top.plot.region <<- 10^par("usr")[4] # top of the region figure (according to y scale)
      if(any(par()$yaxp[1:2] == 0)){
        if(par()$yaxp[1] == 0){
          par(yaxp = c(10^-30, par()$yaxp[2:3])) # because log10(par()$yaxp[1] == 0) == -Inf
        }
        if(par()$yaxp[2] == 0){
          par(yaxp = c(par()$yaxp[1], 10^-30, par()$yaxp[3])) # because log10(par()$yaxp[2] == 0) == -Inf
        }
      }
      axis(side=y.side.fun, at=c(10^par()$usr[3], 10^par()$usr[4]), labels=rep("", 2), lwd=1, lwd.ticks=0) # draw the axis line
      mtext(side = y.side.fun, text = y.lab.fun, line = y.dist.legend, las = 0)
      par(tcl = -par()$mgp[2] * sec.tick.length) # lenght of the ticks are reduced
      suppressWarnings(rug(10^outer(c((log10(par("yaxp")[1])-1):log10(par("yaxp")[2])), log10(1:10), "+"), ticksize = NA, side = y.side.fun)) # ticksize = NA to allow the use of par()$tcl value
      par(tcl = -par()$mgp[2] * tick.length) # back to main tick length
      axis(side = y.side.fun, at = c(1e-15, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9, 1e10), labels = expression(10^-15, 10^-14, 10^-13, 10^-12, 10^-11, 10^-10, 10^-9, 10^-8, 10^-7, 10^-6, 10^-5, 10^-4, 10^-3, 10^-2, 10^-1, 10^0, 10^1, 10^2, 10^3, 10^4, 10^5, 10^6, 10^7, 10^8, 10^9, 10^10), lwd = 0, lwd.ticks = 1, cex.axis = magnific)
      y.text <- 10^(par("usr")[4] + (par("usr")[4] -  par("usr")[3]) / (par("plt")[4] - par("plt")[3]) * (1 - par("plt")[4]))
      mtext(side = y.side.fun, text = y.lab.fun, line = y.dist.legend, las = 0)
    }else{
      mid.bottom.space <<- (par("usr")[3] - ((par("usr")[4] -  par("usr")[3]) / (par("plt")[4] - par("plt")[3])) * par("plt")[3] / 2) # to position axis labeling at the bottom of the graph (according to y scale)
      mid.top.space <<- (par("usr")[4] + ((par("usr")[4] -  par("usr")[3]) / (par("plt")[4] - par("plt")[3])) * par("plt")[3] / 2) # to position axis labeling at the top of the graph (according to y scale)
      top.plot.region <<- par("usr")[4] # top of the region figure (according to y scale)
      axis(side=y.side.fun, at=c(par()$usr[3], par()$usr[4]), labels=rep("", 2), lwd=1, lwd.ticks=0) # draw the axis line
      axis(side=y.side.fun, at=round(seq(par()$yaxp[1], par()$yaxp[2], length.out=par()$yaxp[3]+1), 2))
      mtext(side = y.side.fun, text = y.lab.fun, line = y.dist.legend, las = 0)
      if(y.nb.inter.tick > 0){
        inter.tick.unit <- (par("yaxp")[2] - par("yaxp")[1]) / par("yaxp")[3]
        par(tcl = -par()$mgp[2] * sec.tick.length) # lenght of the ticks are reduced
        suppressWarnings(rug(seq(par("yaxp")[1] - 10 * inter.tick.unit, par("yaxp")[2] + 10 * inter.tick.unit, by =  inter.tick.unit / (1 + y.nb.inter.tick)), ticksize = NA, side=y.side.fun)) # ticksize = NA to allow the use of par()$tcl value
        par(tcl = -par()$mgp[2] * tick.length) # back to main tick length
      }
      y.text <- (par("usr")[4] + (par("usr")[4] -  par("usr")[3]) / (par("plt")[4] - par("plt")[3]) * (1 - par("plt")[4]))
    }
  }else{
    y.text <- (par("usr")[4] + (par("usr")[4] -  par("usr")[3]) / (par("plt")[4] - par("plt")[3]) * (1 - par("plt")[4]))
  }
  par(xpd=NA)
  text(x = x.text, y = y.text, text.corner, adj=c(1, 1.1), cex = magnific.text.corner) # text at the topright corner
  par(xpd=FALSE)
  if(par.ini.fun == TRUE){
    par(tempo.par.ini)
  }
}


################ Closing all the pdf files (but not the R windows)


fun.close.pdf <- function(){
  if(length(dev.list()) != 0){
    for(i in length(names(dev.list())):1){
      if(names(dev.list())[i] == "pdf"){
        dev.off(dev.list()[i])
      }
    }
  }
}


################ Colors of the variant names in the stripcharts and boxplots


fun.color.name <- function(categ, col.wt.reference = "black", col.other.reference = hsv(20/24, 1, 0.75), col.pathogenic = hsv(0, 1, 1), col.neutral = hsv(13/24, 1, 0.9), col.UV = hsv(0, 0, 0.6)){
  # categ: write the levels of the variants (levels(user.data.norm$Name) or levels(res.tot$Variant)
  # col.wt.reference: color of the reference.column
  # col.other.reference: color of Other.reference
  # col.pathogenic: color of the pathogenic variants (red by default)
  # col.neutral: color of the neutral variants (blue by default)
  # col.UV: color of the UV (grey by default)
  color.name.water <<- vector("character", length(categ))
  color.name.water[categ %in% reference.column] <<- col.wt.reference
  if(all(Other.reference != "NO")){
    color.name.water[categ %in% Other.reference] <<- col.other.reference
  }
  color.name.water[categ %in% Pathogenic] <<- col.pathogenic
  color.name.water[categ %in% Neutral] <<- col.neutral
  if(all(UV != "NO")){
    color.name.water[categ %in% UV] <<- col.UV
  }
}


################ Stripcharts in waterfall


fun.stripchart <- function(data1 = output.fun.ordering.waterfall, data2 = NULL, shift = data.shift, central = central.param.displayed, categ.nb = length(output.fun.ordering.waterfall), categ.name = names(output.fun.ordering.waterfall), text.corner.f = ""){
  # require fun.color.name() activated first to get color.name.water
  # embedded: fun.post.graph.feature() which uses the text.corner.f argument
  # data1: a list of values to be plotted. Either splitted user.data.norm.ini or output.fun.ordering.waterfall (default) provided by fun.ordering.waterfall(). The variant order is the same as in the list
  # data2: an optional reduced list of the values to be added in the plot
  # shift: an optional number that shift data1 on the left and data2 on the rigth (unit of the x-axis)
  # central: either median or mean of the data1 displayed (grey horizontal segment)
  # categ.nb: number of categories displayed
  # categ.name: name of the categories displayed
  # text.corner.f: name of the categories displayed
  if(!is.null(data2)){
    jit <- 0
  }else{
    shift <- 0
  }
  par(xaxt="n", xaxs="i")
  stripchart(at = (1:categ.nb) - shift, data1, vertical = TRUE, method = "jitter", jitter = jit, xlim = range(range.min, range.max), xlab = names(obs2)[2], ylab = names(obs2)[1], log = ylog.scale, pch = 1, cex = pt.cex.strip)
  if(!is.null(data2)){
    stripchart(at = (1:categ.nb) + shift, data2, vertical = TRUE,  add = TRUE, col = "red", log = ylog.scale,  pch = 1, cex = pt.cex.strip)
  }
  m <- sapply(data1, central)
  l.seg <- 0.3
  segments((1:categ.nb)- l.seg, m, (1:categ.nb)+ l.seg, m, lwd=4, col=gray(0.4)) # draw medians or means of variants
  par(xaxt = "s", xpd = TRUE)
  axis(side = 1, at = 1:length(m), labels = rep("", length(m)), lwd = 0, lwd.ticks = 1)
  par(xpd = FALSE, yaxt = "s")
  segments(range.min, m[1], range.max, m[1], lty="44") # draw the WT median or mean
  fun.post.graph.feature(ylog.scale = ylog.scale, x.nb.inter.tick = -1, text.corner = text.corner.f)
  par(xpd=TRUE)
  text(x = (1:categ.nb), y = mid.bottom.space, labels = categ.name, col = color.name.water, srt = text.angle, cex = amplif)
  if(!is.null(data2)){
    m2 <- sapply(data2, central)
    segments((1:categ.nb)- l.seg, m2, (1:categ.nb)+ l.seg, m2, lwd=3, col="red") # draw medians or means of reduced variants
    legend("topleft", inset=c(0, -(1 - par("plt")[4]) / (par("plt")[4] - par("plt")[3])), legend = c("Initial", "Reduced"), yjust = 0.5, pch=c(1, 1), col=c("black", "red"), bty="n", cex = amplif, pt.cex = 2, y.intersp=1.25)
  }
  par(xpd = FALSE)
}




